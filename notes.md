# typescript practice

overview

- javascript -> browser / node
- typescript -> javascript compile
- why typescript
- wierd javascript

browser

- dom
- vite

nodejs

- ts-node
- nodemon

important rules

- var, let, const
- ===
- naming (capitalization, camel-case)
- naming (good vs bad names)
- ;

types

- types vs values
- number
- special types (any, unknown)
- enum (number and string)
- function type (+ void, never)
- as, \<type\>value
- instanceof, typeof
- type definitions (type alias, | abd &)
- interface

tools \*

- ... operator
- ?? and || and &&
- A ? B : C
- strings (format strings)

functions \*

- define
- call
- args
- return
- default values (undefined and ?)
- ...rest -
- destructure object
- arrow functions

classes \*

- constructor
- properties (#private)
- methods
- this
- inheritance -> dont bother

async \*

- callbacks
- promises
- async / await

jsdoc \*

- function
- class
- file

modules +

- what are they?
- commonjs
- es modules
- export, default
- import, {}, { A as B }, \*

errors +

- throw
- try / catch

testing \*

- how to write
- how to run

common

- JSON
- Date

dom

- access and change web page
- keyboard listeners

Project steps

- setup
- data and models
- element (button together)
- components (start together, hints for project/subtask)
- interfaces (convert existing and usage: app.ts)
- filesystem utils ()
- testing
- jsdoc
- validation (json)
- css
- fonts

create project

- tsc command
- tsconfig
- @types/
- strict mode
- eslint
- html + css + typescript

Advanced Types

- references
- generics
- react

Project

- overall instructions
- specific filenames / directory structure / git commiting
- multi-file (modules) specify internal and external in instructions
- add hints 'try and use' the things learned in the study sections when I use
  in example implementation
- make sure jsdoc
- have error handling somewhere (save file usin json?)
- theme...: async, classes (run into 'this' problem), functions,

ideas:

- simple web page game
