# Typescript - practice

This practice has 2 parts + a 'playground'

## review

The review doesn't teach you typescript. It asks questions about different
aspects of typescript. If you don't know something, you have to search and
learn about it on your own.

- quetions: start at the beginning and answer the questions until the end...
- solutions: look in here if you give up searching... or you want to check
  your answers.

unless it is too easy, then just skip it!

## project

The project is a guided tour on creating a web application. It does not use
react or any other libary / framework. You will have an oppotunity to use
difference aspects of typescript.

At the beginning, there are more detailed instructions. towards the end,
instructions are more general. Good luck.

If you think you have a better method, or different functionality you want...
it is your project! do whatever you want =)

Run the project completed example

    cd project/time-tracker
    npm ci
    npm run dev

## playground

A place you can write ts code without worring about setting up the environment
first. (there are probably online tools you could use too!)

- index.html -> root html of web app (has an id='root' div you can use)
- src/index.ts -> entry point for typescript code
- public/css/style.css -> you can add styles here

Run the playground

    cd playground
    npm ci
    npm run dev
