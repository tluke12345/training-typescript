# time-tracker - step 4 - Project Component

## overview

- 'Project' component

  - name
  - elapsedTime
  - add subtask button
  - edit button

  - reset button

Todo:

- elapseTime value and formatting
- subtasks
- add subtask action
- save action

## Project

create 'src/components/Project.ts' file

create and export 'Project' class that implements the 'Component' interface

We can now display this instead of the loading component. in 'src/app.ts',
replace loading component with this component

add a private '#html' property. Return this property from Component 'html()'
method. Also, add a constructor and initialize it.

there are many buttons on this screen. add 'onAdd', 'onEdit', 'onReset', and
'onSave' methods

## UI

Lets define the basic components of this screen

- name element
- elapsed element
- subtask column
- add button -> calls 'onAdd'
- edit button -> calls 'onEdit'
- save button -> calls 'onSave'
- reset button -> calls 'onReset'

define these in the constructor and save as private properties

(careful about 'this')

finally! layout the page. (set the #html property)

![Before styles](media/step_4_before_style.png)

## styles

Not very nice.. lets add some styles

- button
- button-circle (add button)
- button-red (reset)
- button-green (save)
- strong (project name and elapse text)
- project (page container)

now we can add these classes to the components we made in the project
constructor

Thats better

![After](media/step_4_after_style.png)

## Edit Mode

with an 'editing' mode, we want to hide/show UI elements sometimes. add a
'hidden' utils css class with !important modifier

add a '#isEditing' private variable. in the 'onEdit', toggle this value.

edit mode -> hide 'save' and 'reset' buttons. change "edit" button title from
'edit' to 'finish'

Now, we need a text field to edit the name. add '#nameField'.

- visible only when editing
- onChange updates the value of 'nameElement'
- hide 'nameElement' when editing

now fix the 'input' styles

## reset

when the 'reset' button is pressed, show a confirmation dialog

if the user confirms, reset the project name
