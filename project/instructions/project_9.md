# time-tracker - step 9 - json and async (Challenge)

## overview

- create filesytem utilities
  - downloadJson
  - filepicker
  - readJsonFile
- json parsing
  - validation
  - json -> project paring
- save json data
- start menu
- load from json

## download json util

create a new file 'src/utils/filesystem.ts'

add 'downloadJson' method.

- async
- input: data (any)
- input: filename (string)

## export to json

add 'data' method to subtask that returns object with current state data

add 'data' method to project that returns object with project state including
subtasks

update 'onSave' method.

## Importing Json (prep)

### file-picker util

add 'showFilePicker' utility method

- async
- no input
- return 'File' or unknown promise

You will probably have to convert a 'callback' style api into a promise so that
we can use it with async / await

### read json from file util

add 'readJson' utility function

- async
- input: 'File' object
- output: 'any' returned in a promise

You will probably have to convert a 'callback' style api into a promise so that
we can use it with async / await

### validate json data

We get an "any" object when reading a file. we need to check that it is correct
and that we know how to read it.

add 'src/utils/validation.ts'

create a validation method 'isProjectData'

- input: 'any' object
- output: TYPE GUARD (obj is ProjectData)

use 'typeof', 'instanceof', etc, to confirm the data is the correct shape.

You will also have to define some data type or interface that we are expecting

### parse json utility

create a file 'src/utils/parseProject.ts'.

create and export function "parseProjectJson"

- input: 'json', and object of type 'any'
- output: Project object (throw an error if any problems)

### constructors with intial values

add optional arguments to Project constructor

- name, an optional string
- subtasks, an optional array of Subtask objects

don't forget to initialize the UI with the values if avaiable

add optional arguments to Subtask constructor

- name, an optional string
- intervals, an optional array of TimeInterval objects

don't forget to initialize the UI with the values if avaiable. now there are 3
arguments, all optional, this is a good time to change it to a
'destructured object' (you can easily do this with VSCode refactor tools!)

## Importing Json (implementation) - start menu

We don't have anywhere to 'load' our json files... Instead of the initial UI of
an empty project, lets add a 'start menu' that asks us if we want to start a
new project or load from json

### general steps

- create 'src/components/Start.ts'
- create a 'Component' that displays 2 buttons: "new project" and "load json"
  - new -> show Project UI with empty data (just like now)
  - load ->

### Difficult points

How do we show the UI?
error handling
async

### completed component

style

## update app

set "Start" as the initial component instead of "Project"

## done...?

Some small things that need to be done:

- prompt for filename when saving json file
- jsdoc on Components and Elements
- more tests? how can you test UI?
- custom design?
- limit currenly running task to 1
- multiple projects?
- save current state to local storage or cookies?
- anything else you want!
