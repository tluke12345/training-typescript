# time-tracker - step 1 - setup

## Overview

- create project
- add typescript
- add vite
- add stylelint
- add index.html, styles.css, main.ts
- add fonts
- set up css variables
- add eslint
- add prettier
- setup tests
- add package scripts

## create project

create directory named 'time-tracker'

initialize git. set initial branch to 'main' (not 'master').

add .gitignore (make sure to ignore node_modules)

initialize npm package.

## typescript

Add typescript (typescript is a dev dependency)

initialize tsconfig

## vite

Add vite

## Stylelint

Add stylelint

create stylelint config file

extend config-standard

## Index / styles

Add index.html

- set title 'time tracker'
- add a 'root' div

create 'src/main.ts' and add to html as 'module' script

Add 'src/public/css/style.css' file.

add reset and basic font styles

- padding 0, margin 0, box-sizing of border box to all elements

Add css variables

| variable           | light   | dark    |
| ------------------ | ------- | ------- |
| color-text         | #1f1f1f | #ffffff |
| color-bg           | #fdfdfd | #1f1f1f |
| color-gray-bg      | #faf8f8 | #292929 |
| color-gray-hover   | #dadada | #515151 |
| color-gray-border  | #dadada | #4e4e4e |
| color-green-bg     | #afedae | #24602e |
| color-green-hover  | #64df61 | #238632 |
| color-green-border | #17c008 | #0a6708 |
| color-green-tint   | #eaf3ea | #1d2b1c |
| color-red-bg       | #ffcdcd | #532a2a |
| color-red-hover    | #ff9393 | #792020 |
| padding-lg         | 1.5rem  |         |
| padding-md         | 1rem    |         |
| padding-sm         | 0.5rem  |         |
| border-radius-lg   | 0.5rem  |         |
| border-radius-sm   | 0.24rem |         |
| height-button      | 2rem    |         |
| height-subtask     | 3rem    |         |
| width-project      | 800px   |         |
| width-start        | 10rem   |         |
| letter-spacing     | 1px     |         |

Add Noto-sans-jp font

set the following font to the whole page

| name                | value                      |
| ------------------- | -------------------------- |
| font-family         | "Noto Sans JP", sans-serif |
| font-optical-sizing | auto                       |
| font-weight         | normal                     |
| font-style          | normal                     |
| letter-spacing      | letter spacing variable    |
| name| ------------------- | -------------------------- |
| font-family| font-optical-sizing | auto| font-weight| font-style| letter-spacing

Add favicon (create or copy into 'public/img/')

### test everything is working

Add src/main.ts with a "hello world" typescript function (print to console)

test that vite and typescript are working (npx)

Check:

- console log printed (from main.ts)
- styles have loaded
- favicon works

## lint (eslint)

add eslint to project

initialize eslint

test eslint is working. Add error to main.ts (unused variable, etc.)

## formatting (prettier)

add prettier to project

configure prettier

check that prettier format all files from cli command

## test

Add 'vitest' to project

create a simple 'sum' util in 'src/utils/sum.ts'. it should add two numbers
and return the results

Add "src/utils/sum.test.ts". write a test to check 'sum' function

run the test (npx)

## scripts

add "dev" script to package.json

add "lint" script to package.json

add "build" script to package.json

add "format" script to package.json (format all files)

add "lint:fix" script to package.json (use eslint --fix)

add "dist" directory to .gitignore

add "test" script to package.json

## troubleshooting

hints

eslint --init options:

- problems
- esm
- none
- typescript
- browser

eslint doesn't install dependencies properly -> install by hand

    (adjust versions if required)
    (I ended up uninstalling all eslint and then reinstalling)

    npm i -D typescript-eslint @eslint/js eslint
