# time-tracker - step 5 - Subtask Component

## overview

- Subtask
  - name
  - name field
  - start button
  - elapseTime
- styles
- edit mode
- running mode

TODO

- state
- elapseTime value and formatting

## General Steps

### Create Component

### button handlers

### Create elements

### Create UI

### (temp) add a subtask to Project so we can see it

### add styles

### implement editing mode

### implement running mode
