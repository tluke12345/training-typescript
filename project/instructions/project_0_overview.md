# project

## problem

I have a hard time keeping track of how much time I have spent on each sub-task
when I work on a project. I sometimes go back and forth between multiple
sub-tasks. I want a way to keep track of the time I spend on each task.

## solution idea

a project time tracker.

- set project name
- project total time spent is displayed
- multiple sub-tasks
- each sub-task has name, time spent, and start/stop button
- sub-tasks can be added
- project / sub-task names can be edited
- everything can be reset
- save and load from local json files

## concept

![alt concept light](media/concept-light.png "Light")
![alt concept dark](media/concept-dark.png "Dark")

## design

| variable           | light   | dark    |
| ------------------ | ------- | ------- |
| color-text         | #1f1f1f | #ffffff |
| color-bg           | #fdfdfd | #1f1f1f |
| color-gray-bg      | #faf8f8 | #292929 |
| color-gray-hover   | #dadada | #515151 |
| color-gray-border  | #dadada | #4e4e4e |
| color-green-bg     | #afedae | #24602e |
| color-green-hover  | #64df61 | #238632 |
| color-green-border | #17c008 | #0a6708 |
| color-green-tint   | #eaf3ea | #1d2b1c |
| color-red-bg       | #ffcdcd | #532a2a |
| color-red-hover    | #ff9393 | #792020 |
| padding-lg         | 1.5rem  |         |
| padding-md         | 1rem    |         |
| padding-sm         | 0.5rem  |         |
| border-radius-lg   | 0.5rem  |         |
| border-radius-sm   | 0.24rem |         |
| height-button      | 2rem    |         |
| height-subtask     | 3rem    |         |
| width-project      | 800px   |         |
| width-start        | 10rem   |         |
| letter-spacing     | 1px     |         |

## project challenge Levels

### hardest

use this page as project 'requirements' and create the app by yourself.

Additional requirements:

- no dependencies (dev-dependencies OK)
- use typescript
- no warnings
- include some of the following:
  - tests
  - jsdocs
  - async
  - interfaces
  - destructured object function params

### hard

refer to each of the project instruction files. At the beginning of each step,
there is an 'overview section'. Finish the step by yourself.

### custom

Important: **you can ask question or for explination any time!**

- read the overview and try to do it by yourself.
- if you get stuck, read the step by step instructions. Research online if you 
  don't know how to do something.
- still not working? look at the hints or answers.
- last resort -> look at the solution source code (if available)
