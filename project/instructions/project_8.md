# time-tracker - step 8 - clock

## overview

- add 'state' to the 'Subtask' component
- wire up the subtask button
- update the elapse time UI
- update the project elapse time UI

## subtask state

Add 'startTimeStamp' which is a number (Date as milliseconds) if running,
undefined if not running

Add 'intervals' which holds an array of the previous time durations that the
subtask was run

Handle 'start' and 'stop'

- Set 'startTime' when isRunning is set to true.
- Set save a new interval and set 'startTime' to undefined when isRunning is
  false

## subtask elapseTime

create a method 'elapseTime' that returns the current elapse time for this
subtask.

when subtask is set to running ('start' method), set UI to 'seconds' style
string

when subtask is set to stopped ('stop' method), set UI to 'minutes' style
string

when subtask is updated, we are running, so update the UI to 'seconds' style
string

## project elapseTime

every time the project is 'updated', update the string displayed for the total
elapse time

## Test everything

Run the app. start multiple task:

- make sure "0:00:00" format while running
- make sure "0:00" format while stopped
- make sure elapse time value updates every second
- make sure the total elapse time is correct and updates (this will take a few
  minutes)
