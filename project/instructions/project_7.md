# time-tracker - step 7 - time utilities

## outline

utility functions to:

- calculate elapse time
- convert elapse time to seconds string ("0:00:00")
- convert elapse time to minutes string ("0:00")

## TimeInterval

Create a new type called 'TimeInterval'. put it in 'src/models/'

- startTime
- endTime

This will be used to store the multiple periods of work for each subtask.

add jsdoc comments

## clean up

remove 'src/sum.ts'

remove 'src/sum.test.ts'

## calculate elapse time

create file 'src/utils/timeUtils.ts'

create a function 'calculateElapseTime'

- input: array of time intervals
- returns: the elase time for that interval

create a file 'src/utils/timeUtils.test.ts'

test the 'calculateElapseTime' function. Make sure to check the following cases:

- interval with elapse time of zero
- interval with negative elapse time (elapse time should always be positive)
- elapse time is always an integer (check rounding limits)

test cases:

| case              | startTime    | endTime        | expected result |
| ----------------- | ------------ | -------------- | --------------- |
| zero              | 0            | 0              | 0               |
| postive range     | 1000         | 2222           | 1222            |
| postive range     | 2000         | 1111           | 0               |
| non-integer range | non-rounding | 1000.1, 2000.2 | 1000            |
| non-integer range | rounding     | 1000.3, 2000.2 | 999             |

test

run the tests

add jsdoc to the 'calculateElapseTime' function

## calculate total elapse time

create another function in 'src/utils/timeUtils.ts' called
'calculateTotalElapseTime'

test the 'calculateTotalElapseTime' function

- empty array
- single interval
- multiple intervals
- intervals of different values

test cases

| case                        | intervals                    | expected result |
| --------------------------- | ---------------------------- | --------------- |
| empty array                 | []                           | 0               |
| single interval             | i(1000, 2222)                | 1222            |
| multiple intervals          | i(1000, 2222), i(4000, 4234) | 1456            |
| handle zero value intervals | i(1000, 2222), i(4500, 4234) | 1222            |

tests

add jsdocs to 'calculateTotalElapseTime' function

## calculate seconds display strings

write 'convertElapseTimeToSecondsString' method

- input: number, positive, integer, milliseconds
- return: string like '1:04:11'
- jsdoc
- test

## calculate minutes display strings

write 'convertElapseTimeToMinutesString' method

- input: number, positive, integer, milliseconds
- return: string like '1:04'
- jsdoc
- test

## troubleshooting note

if you try and use the 'padStart' method and you get an error
"Property 'padStart' does not exist on type string...." you need to update
you tsconfig.json file:

    // tsconfig.json
    {
        "compilerOptions": {
            "target": "es2017"
        }
    }
