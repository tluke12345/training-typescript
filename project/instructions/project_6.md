# time-tracker - step 6 - Add Subtasks to Project

## overview

- Add subtasks to Project Component
  - add subtasks
  - edit subtasks
  - reset subtasks
  - update subtasks

## general steps

### subtasks array

### add subtasks internal property

### pass editing state to subtasks

### pass updates to subtasks

### reset subtasks
