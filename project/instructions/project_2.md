# time-tracker - step 2 - Elements

## overview

- button element
- column element
- row element
- text element
- textField element
- button element
- flexibleSpace element

## element creation step-by-step

Review the 'section 13 - dom intro'.

Create a file 'src/elements/button.ts'

Create and export a function named 'button' (use 'function' keyword)

Create a button using dom API and return it from the function

- title of "a button"
- class of "button"
- console.log("button clicked") when clicking on button

Set the return type of the function

Use the 'button' element in main.js. Add a button to the 'root' div.

start vite server to see the button

adjust the css styles (using javascript) to change the style

add a 'title' argument to the 'button' element. use it to set the button text

we want to pass a function as an argument and set the 'onclick' to call that
function. What is the type of a function that takes no arguments and returns
no values.

add an 'onclick' argument to the 'button' element

update the main.ts so it works again

remove styles (we don't need them here) and set 'button' css class

'button' element is done!

## Other elements

### column

- takes: 'children', a list of HTMLElements
- returns: an HTMLElement

use 'flexbox' to layout the children

### row

- takes: 'children', a list of HTMLElements
- returns: an HTMLElement

use 'flexbox' to layout the children

### text

- takes: 'value', as string to display
- returns: an HTMLElement

### flexibleSpacer

This is an object we can put into a row or column and will expand as much as
possible

- takes: no arguments
- return: an HTMLElement

use the 'flex' css property

### textField (challenge)

- takes: 'placeholder', a string (optional)
- takes: 'initial', a string to set as the initial value (optional)
- takes: 'onChange', a function that is called
  - onChange takes 'value' an optional string and returns nothing
- returns: an HTMLInputElement

use 'object desctructuring' so that we can use named arguments

## Use the elements

in the 'main.ts' file, create the following html using the newly created
elements

- 'nav bar'
  - left: "Logo" text
  - right:
- 'content'
  - "name" text field with "name" placeholder. print to console when updated
  - "email" text field with "email" placeholder. print to console when updated

or, a written differently (easier to configure!)

![Step2 - before](media/step_2_before.png)

we can add a few styles and it is actually usuable now!

![Step2 - after](media/step_2_after.png)
