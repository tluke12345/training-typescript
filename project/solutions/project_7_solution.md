# time-tracker - step 7 - time utilities

## outline

utility functions to:

- calculate elapse time
- convert elapse time to seconds string ("0:00:00")
- convert elapse time to minutes string ("0:00")

## TimeInterval

Create a new type called 'TimeInterval'. put it in 'src/models/'

- startTime
- endTime

This will be used to store the multiple periods of work for each subtask.

    export type TimeInterval = {
      startTime: number;
      endTime: number;
    }

add jsdoc comments

    /**
     * Time Inteval
     */
    export type TimeInterval = {
      /** intervals starting time in milliseconds */
      startTime: number;

      /** intervals ending time in milliseconds */
      endTime: number;
    };

## clean up

remove 'src/sum.ts'

remove 'src/sum.test.ts'

## calculate elapse time

create file 'src/utils/timeUtils.ts'

create a function 'calculateElapseTime'

- input: array of time intervals
- returns: the elase time for that interval

like this

    export function elapseTime(interval: TimeInterval): number {
      const raw = interval.endTime - interval.startTime;
      const toPositive = Math.max(0, raw);
      const toInteger = Math.floor(toPositive);
      return toInteger;
    }

create a file 'src/utils/timeUtils.test.ts'

test the 'calculateElapseTime' function. Make sure to check the following cases:

- interval with elapse time of zero
- interval with negative elapse time (elapse time should always be positive)
- elapse time is always an integer (check rounding limits)

test cases:

| case              | startTime    | endTime        | expected result |
| ----------------- | ------------ | -------------- | --------------- |
| zero              | 0            | 0              | 0               |
| postive range     | 1000         | 2222           | 1222            |
| postive range     | 2000         | 1111           | 0               |
| non-integer range | non-rounding | 1000.1, 2000.2 | 1000            |
| non-integer range | rounding     | 1000.3, 2000.2 | 999             |

test

    import { describe, it, expect } from "vitest";
    import { TimeInterval } from "../models/TimeInterval";
    import { calculateElapseTime } from "./timeUtils";

    const i = (from: number, to: number) => {
      return {
        startTime: from,
        endTime: to,
      } as TimeInterval;
    };

    describe("timeUtils", () => {
      describe("calculateElapseTime", () => {
        [
          ["zero", i(0, 0), 0],
          ["postive range", i(1000, 2222), 1222],
          ["negative range", i(2000, 1111), 0],
          ["non-integer range, non-rounding", i(1000.1, 2000.2), 1000],
          ["non-integer range, rounding", i(1000.3, 2000.2), 999],
        ].forEach(([msg, testInterval, expected]) => {
          it(`has expected output for ${msg}`, () => {
            const elapse = calculateElapseTime(testInterval as TimeInterval);
            expect(elapse).toBe(expected);
          });
        });
      });
    })

run the tests

    npm run test

add jsdoc to the 'calculateElapseTime' function

    /**
     * determine the elapse time for a given TimeInterval
     *
     * @param { TimeInterval } interval whose elapse time will be calculated
     * @returns { number } elapse time in milliseconds, positive integer
     */

## calculate total elapse time

create another function in 'src/utils/timeUtils.ts' called
'calculateTotalElapseTime'

    export function calculateTotalElapseTime(intervals: TimeInterval[]): number {
      return intervals
        .map((interval) => calculateElapseTime(interval))
        .reduce((prev, curr) => prev + curr, 0);
    }

test the 'calculateTotalElapseTime' function

- empty array
- single interval
- multiple intervals
- intervals of different values

test cases

| case                        | intervals                    | expected result |
| --------------------------- | ---------------------------- | --------------- |
| empty array                 | []                           | 0               |
| single interval             | i(1000, 2222)                | 1222            |
| multiple intervals          | i(1000, 2222), i(4000, 4234) | 1456            |
| handle zero value intervals | i(1000, 2222), i(4500, 4234) | 1222            |

tests

    describe("calculateTotalElapseTime", () => {
      [
        ["empty array", [], 0],
        ["single interval", [i(1000, 2222)], 1222],
        ["multiple intervals", [i(1000, 2222), i(4000, 4234)], 1456],
        ["handle zero value intervals", [i(1000, 2222), i(4500, 4234)], 1222],
      ].forEach(([msg, testIntervals, expected]) => {
        it(`has expected output for ${msg}`, () => {
          const elapse = calculateTotalElapseTime(testIntervals as TimeInterval[]);
          expect(elapse).toBe(expected);
        });
      });
    });

add jsdocs to 'calculateTotalElapseTime' function

    /**
     * determine the total elapse time for all given TimeIntervals
     *
     * @param { TimeInterval[] } intervals whose total elapse time will be calculated
     * @returns { number } total elapse time in milliseconds, positive integer
     */

## calculate seconds display strings

write 'convertElapseTimeToSecondsString' method

- input: number, positive, integer, milliseconds
- return: string like '1:04:11'
- jsdoc
- test

like this

    /**
     * Create a human readable string from elapseTime number
     *
     * example: 5515000 -> "1:31:55" (1 hour, 31 min, 55 sec)
     *
     * @param { number } elapseTime number of milliseconds to be converted
     * @return { string } human readable string
     */
    export function convertElapseTimeToSecondsString(elapseTime: number): string {
      if (elapseTime < 0) {
        return "--:--:--";
      }
      try {
        const totalSeconds = Math.floor(elapseTime / 1000);
        const seconds = totalSeconds % 60;
        const secondsStr = seconds.toFixed(0).padStart(2, "0");
        const totalMinutes = Math.floor(totalSeconds / 60);
        const minutes = totalMinutes % 60;
        const mintessStr = minutes.toFixed(0).padStart(2, "0");
        const hours = Math.floor(totalMinutes / 60);
        return `${hours}:${mintessStr}:${secondsStr}`;
      } catch (e) {
        console.error("error converting elapsed time into display string: ", e);
        return "--:--:--";
      }
    }

    describe("convertElapseTimetoSecondsString", () => {
      [
        ["zero", 0, "0:00:00"],
        ["minus value", -10000, "--:--:--"],
        ["seconds only", 10000, "0:00:10"],
        ["right before 2 minutes", 119000, "0:01:59"],
        ["just 2 minutes", 120000, "0:02:00"],
        ["right before 1 hour", 3599000, "0:59:59"],
        ["just 1 hour", 3600000, "1:00:00"],
        ["mix of hours and minutes", 36456000, "10:07:36"],
        ["100 hours or more", 360120000, "100:02:00"],
      ].forEach(([msg, interval, expected]) => {
        it(`has expected output for ${msg}`, () => {
          const actual = convertElapseTimeToSecondsString(interval as number);
          expect(actual).toBe(expected as string);
        });
      });
    });

## calculate minutes display strings

write 'convertElapseTimeToMinutesString' method

- input: number, positive, integer, milliseconds
- return: string like '1:04'
- jsdoc
- test

like this

    /**
     * Create a human readable string from elapseTime number
     *
     * example: 5515000 -> "1:31" (1 hour, 31 min)
     *
     * @param { number } elapseTime number of milliseconds to be converted
     * @return { string } human readable string
     */
    export function convertElapseTimeToMinuteString(
      elapseTime: number,
    ): string {
      if (elapseTime < 0) {
        return "--:--";
      }
      try {
        const totalMinutes = Math.floor(elapseTime / 1000 / 60);
        const minutes = totalMinutes % 60;
        const mintessStr = minutes.toFixed(0).padStart(2, "0");
        const hours = Math.floor(totalMinutes / 60);
        return `${hours}:${mintessStr}`;
      } catch (e) {
        console.error("error converting elapsed time into display string: ", e);
        return "--:--";
      }
    }

    describe("convertElapseTimetoMinutesString", () => {
      [
        ["zero", 0, "0:00"],
        ["minus value", -10000, "--:--"],
        ["seconds only", 10000, "0:00"],
        ["right before 2 minutes", 119000, "0:01"],
        ["just 2 minutes", 120000, "0:02"],
        ["right before 1 hour", 3599000, "0:59"],
        ["just 1 hour", 3600000, "1:00"],
        ["mix of hours and minutes", 36456000, "10:07"],
        ["100 hours or more", 360120000, "100:02"],
      ].forEach(([msg, interval, expected]) => {
        it(`has expected output for ${msg}`, () => {
          const actual = convertElapseTimeToMinuteString(interval as number);
          expect(actual).toBe(expected as string);
        });
      });
    });

## troubleshooting note

if you try and use the 'padStart' method and you get an error
"Property 'padStart' does not exist on type string...." you need to update
you tsconfig.json file:

    // tsconfig.json
    {
        "compilerOptions": {
            "target": "es2017"
        }
    }
