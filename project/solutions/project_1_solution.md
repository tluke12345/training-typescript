# time-tracker - step 1 - setup

## Overview

- create project
- add typescript
- add vite
- add stylelint
- add index.html, styles.css, main.ts
- add fonts
- set up css variables
- add eslint
- add prettier
- setup tests
- add package scripts

## create project

create directory named 'time-tracker'

initialize git. set initial branch to 'main' (not 'master').

    git init -b main

add .gitignore (make sure to ignore node_modules)

    // .gitignore
    node_modules

initialize npm package.

    npm init -y

## typescript

Add typescript (typescript is a dev dependency)

    npm i typescript@latest -D

initialize tsconfig

    npx tsc --init

## vite

Add vite

    npm i vite -D

## Stylelint

Add stylelint

    npm i stylelint -D
    npm i stylelint-config-standard -D

create stylelint config file

    touch .stylelintrc.json

extend config-standard

    // .stylelintrc.json
    {
        "extends": "stylelint-config-standard"
    }

## Index / styles

Add index.html

- set title 'time tracker'
- add a 'root' div

something like

    <!doctype html>
    <html lang="en">

    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>time tracker</title>
    </head>

    <body>
      <div id="root"></div>
    </body>

    </html>

create 'src/main.ts' and add to html as 'module' script

      <script type="module" src="src/main.ts"></script>

Add 'src/public/css/style.css' file.

    <link rel="stylesheet" href="/css/styles.css" />

add reset and basic font styles;

- padding 0, margin 0, box-sizing of border box to all elements

Add css variables

| variable           | light   | dark    |
| ------------------ | ------- | ------- |
| color-text         | #1f1f1f | #ffffff |
| color-bg           | #fdfdfd | #1f1f1f |
| color-gray-bg      | #faf8f8 | #292929 |
| color-gray-hover   | #dadada | #515151 |
| color-gray-border  | #dadada | #4e4e4e |
| color-green-bg     | #afedae | #24602e |
| color-green-hover  | #64df61 | #238632 |
| color-green-border | #17c008 | #0a6708 |
| color-green-tint   | #eaf3ea | #1d2b1c |
| color-red-bg       | #ffcdcd | #532a2a |
| color-red-hover    | #ff9393 | #792020 |
| padding-lg         | 1.5rem  |         |
| padding-md         | 1rem    |         |
| padding-sm         | 0.5rem  |         |
| border-radius-lg   | 0.5rem  |         |
| border-radius-sm   | 0.24rem |         |
| height-button      | 2rem    |         |
| height-subtask     | 3rem    |         |
| width-project      | 800px   |         |
| width-start        | 10rem   |         |
| letter-spacing     | 1px     |         |

    :root {
      /* colors (DARK) */
      --color-text: #ffffff;
      --color-bg: #1f1f1f;
      --color-gray-bg: #292929;
      --color-gray-hover: #515151;
      --color-gray-border: #4e4e4e;
      --color-green-bg: #24602e;
      --color-green-hover: #238632;
      --color-green-border: #0a6708;
      --color-green-tint: #1d2b1c;
      --color-red-bg: #532a2a;
      --color-red-hover: #792020;

      /* colors (LIGHT) */
      --color-text: #1f1f1f;
      --color-bg: #fdfdfd;
      --color-gray-bg: #faf8f8;
      --color-gray-hover: #dadada;
      --color-gray-border: #dadada;
      --color-green-bg: #afedae;
      --color-green-hover: #64df61;
      --color-green-border: #17c008;
      --color-green-tint: #eaf3ea;
      --color-red-bg: #ffcdcd;
      --color-red-hover: #ff9393;

      /* common */
      --padding-lg: 1.5rem;
      --padding-md: 1rem;
      --padding-sm: 0.5rem;
      --border-radius-lg: 0.5rem;
      --border-radius-sm: 0.24rem;

      /* element sizes */
      --height-button: 2rem;
      --height-subtask: 3rem;
      --width-project: 800px;
      --width-start: 10rem;

      /* font */
      --letter-spacing: 1px;
    }

Add Noto-sans-jp font

    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100..900&display=swap" rel="stylesheet" />

set the following font to the whole page

| name                | value                      |
| ------------------- | -------------------------- |
| font-family         | "Noto Sans JP", sans-serif |
| font-optical-sizing | auto                       |
| font-weight         | normal                     |
| font-style          | normal                     |
| letter-spacing      | letter spacing variable    |

like this

    * {
      /* reset */
      padding: 0;
      margin: 0;
      box-sizing: border-box;

      /* Font */
      color: var(--color-text);
      font-family: "Noto Sans JP", sans-serif;
      font-optical-sizing: auto;
      font-weight: normal;
      font-style: normal;
      letter-spacing: var(--letter-spacing);
    }

    body {
      background: var(--color-bg);
      font-size: var(--font-size-base);
    }

Add favicon (create or copy into 'public/img/')

    <link rel="icon" type="image/svg+xml" href="/img/favicon.svg">
    <link rel="icon" type="image/png" href="/img/favicon.png">

### test everything is working

Add src/main.ts with a "hello world" typescript function (print to console)

    function printHello(name: string) {
      console.log("hello", name);
    }
    printHello("travis");

test that vite and typescript are working (npx)

    npx vite

Check:

- console log printed (from main.ts)
- styles have loaded
- favicon works

## lint (eslint)

add eslint to project

    npm i eslint -D

initialize eslint

    npx eslint --init

test eslint is working. Add error to main.ts (unused variable, etc.)

    // add unused variable to main.ts

    // check that error shows in editor

    // check that error shows in following command
    npx eslint ./src

## formatting (prettier)

add prettier to project

    npm i prettier -D

configure prettier

    // .prettierrc.json
    { }

check that prettier format all files from cli command

    // add some extra spaces in main.ts
    npx prettier --write *

## test

Add 'vitest' to project

    npm i -D vitest

create a simple 'sum' util in 'src/utils/sum.ts'. it should add two numbers
and return the results

    // src/utils/sum.ts
    export function sum(a: number, b: number): number {
      return a + b;
    }

Add "src/utils/sum.test.ts". write a test to check 'sum' function

    import { describe, it, expect } from "vitest";
    import { sum } from "./sum";

    describe("App", () => {
      it("sums up two numbers", () => {
        expect(sum(1, 2)).toBe(3);
      });
    });

run the test (npx)

    npx vitest run

## scripts

add "dev" script to package.json

    // package.json, "scripts" array
    "dev": "vite"

add "lint" script to package.json

    // package.json, "scripts" array
    "lint": "eslint ./src"

add "build" script to package.json

    // package.json, "scripts" array
    "build": "vite build"

add "format" script to package.json (format all files)

    // package.json, "scripts" array
    "format": "prettier --write *"

add "lint:fix" script to package.json (use eslint --fix)

    // package.json, "scripts" array
    "lint:fix": "vite build"

add "dist" directory to .gitignore

    // .gitignore
    dist

add "test" script to package.json

    // package.json, "scripts" array
    "test": "vitest run"

## troubleshooting

hints

eslint --init options:

- problems
- esm
- none
- typescript
- browser

eslint doesn't install dependencies properly -> install by hand

    (adjust versions if required)
    (I ended up uninstalling all eslint and then reinstalling)

    npm i -D typescript-eslint @eslint/js eslint
