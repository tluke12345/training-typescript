# time-tracker - step 9 - json and async (Challenge)

## overview

- create filesytem utilities
  - downloadJson
  - filepicker
  - readJsonFile
- json parsing
  - validation
  - json -> project paring
- save json data
- start menu
- load from json

## download json util

create a new file 'src/utils/filesystem.ts'

add 'downloadJson' method.

- async
- input: data (any)
- input: filename (string)

like this

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Export async function downloadJson(data: any, filename: string) {
      const file = new Blob([data], { type: "json" });
      const a = document.createElement("a");
      const url = URL.createObjectURL(file);
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      setTimeout(() => {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }

## export to json

add 'data' method to subtask that returns object with current state data

    data() {
      return {
        name: this.#nameField.value,
        startTime: this.startTime,
        intervals: this.intervals,
      };
    }

add 'data' method to project that returns object with project state including
subtasks

    data() {
      return {
        name: this.#nameField.value,
        subtasks: this.#subtasks.map((subtask) => subtask.data())
      }
    }

update 'onSave' method.

    onSave() {
      const json = JSON.stringify(this.data());
      const filename = `${this.#nameElement.textContent} - ${new Date()}.json`;
      downloadJson(json, filename);
    }

## Importing Json (prep)

### file-picker util

add 'showFilePicker' utility method

- async
- no input
- return 'File' or unknown promise

You will probably have to convert a 'callback' style api into a promise so that
we can use it with async / await

    export async function showFilePicker(): Promise<File | undefined> {
      return new Promise<File | undefined>((res, reject) => {
        const fileInput = document.createElement("input");
        fileInput.type = "file";
        fileInput.onchange = () => {
          if (!fileInput.files || !fileInput.files[0]) {
            reject("Please select a file before clicking 'Load'");
            return;
          }
          res(fileInput.files[0]);
        };
        fileInput.click();
      });
    }

### read json from file util

add 'readJson' utility function

- async
- input: 'File' object
- output: 'any' returned in a promise

You will probably have to convert a 'callback' style api into a promise so that
we can use it with async / await

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    export async function readJsonFile(file: File): Promise<any> {
      return new Promise((res, reject) => {
        const reader = new FileReader();
        reader.onload = (ev: ProgressEvent<FileReader>) => {
          try {
            const json = JSON.parse(ev.target!.result as string);
            res(json);
          } catch (e) {
            reject("Error parsing JSON");
          }
        };
        reader.onerror = () => {
          reject("Error reading file");
        };
        reader.readAsText(file);
      });
    }

### validate json data

We get an "any" object when reading a file. we need to check that it is correct
and that we know how to read it.

add 'src/utils/validation.ts'

create a validation method 'isProjectData'

- input: 'any' object
- output: TYPE GUARD (obj is ProjectData)

use 'typeof', 'instanceof', etc, to confirm the data is the correct shape.

You will also have to define some data type or interface that we are expecting

    // ----------------------------------------------------------------------------
    // Data Interfaces
    // ----------------------------------------------------------------------------
    export interface TimeIntervalData {
      startTime: number;
      endTime: number;
    }
    
    export interface SubtaskData {
      name: string;
      intervals: TimeIntervalData[];
    }
    
    export interface ProjectData {
      name: string;
      subtasks: SubtaskData[];
    }
    
    // ----------------------------------------------------------------------------
    // Type Guards
    // ----------------------------------------------------------------------------
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    export function isTimeIntervalData(obj: any): obj is TimeIntervalData {
      return (
        obj && typeof obj.startTime === "number" && typeof obj.endTime === "number"
      );
    }
    
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    export function isSubtaskData(obj: any): obj is SubtaskData {
      return (
        obj &&
        typeof obj.name === "string" &&
        Array.isArray(obj.intervals) &&
        obj.intervals.every(isTimeIntervalData)
      );
    }
    
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    export function isProjectData(obj: any): obj is ProjectData {
      return (
        obj &&
        typeof obj.name === "string" &&
        Array.isArray(obj.subtasks) &&
        obj.subtasks.every(isSubtaskData)
      );
    }

### parse json utility

create a file 'src/utils/parseProject.ts'.

create and export function "parseProjectJson"

- input: 'json', and object of type 'any'
- output: Project object (throw an error if any problems)

like this

    import { Project } from "../components/Project";
    import { Subtask } from "../components/Subtask";
    import { TimeInterval } from "../models/TimeInterval";
    import { isProjectData } from "./validation";
    
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    export function parseProjectJson(json: any): Project {
      if (!isProjectData(json)) {
        throw new Error("invalid JSON format");
      }
    
      return new Project(
        json.name,
        json.subtasks.map(
          (subtaskData) =>
            new Subtask({
              name: subtaskData.name,
              intervals: subtaskData.intervals.map(
                (timeIntervalData) =>
                  ({
                    startTime: timeIntervalData.startTime,
                    endTime: timeIntervalData.endTime,
                  }) as TimeInterval,
              ),
            }),
        ),
      );
    }

### constructors with intial values

add optional arguments to Project constructor

- name, an optional string
- subtasks, an optional array of Subtask objects

don't forget to initialize the UI with the values if avaiable

    // src/components/Project.ts
    constructor(name?: string, subtasks?: Subtask[]) {
      this.#subtasks = subtasks ?? [];
      this.#nameElement = text(name ?? "name");
      // ..

      this.#nameField = textField({
        placeholder: "project name",
        initial: name,
        onChange: (value) => (this.#nameElement.textContent = value ?? ""),
      });
      // ..

      this.#subtaskColumn = column(
        (subtasks ?? []).map((subtask) => subtask.html()),
      );
      // ..
    }

add optional arguments to Subtask constructor

- name, an optional string
- intervals, an optional array of TimeInterval objects

don't forget to initialize the UI with the values if avaiable. now there are 3 
arguments, all optional, this is a good time to change it to a 
'destructured object' (you can easily do this with VSCode refactor tools!)

    constructor({
      isEditing = false,
      name,
      intervals,
    }: {
      isEditing?: boolean;
      name?: string;
      intervals?: TimeInterval[];
    } = {}) {
      this.intervals = intervals ?? [];
      this.#nameElement = text(name ?? "Subtask");
      this.#nameField = textField({
        placeholder: "subtask name",
        initial: name,
        onChange: (value) => (this.#nameElement.textContent = value ?? ""),
      });
      // ..

      this.#elapseElement = text("0:00");
      if (intervals !== undefined && intervals.length > 0) {
        const elapsedString = convertElapseTimeToMinuteString(this.elapseTime());
        this.#elapseElement.textContent = elapsedString;
      }
      // ..
    }

## Importing Json (implementation) - start menu

We don't have anywhere to 'load' our json files... Instead of the initial UI of
an empty project, lets add a 'start menu' that asks us if we want to start a
new project or load from json

### general steps

- create 'src/components/Start.ts'
- create a 'Component' that displays 2 buttons: "new project" and "load json"
  - new -> show Project UI with empty data (just like now)
  - load ->
    - show File picker
    - load json file data
    - validate json
    - create project instance
    - show Project UI with loaded data

### Difficult points

How do we show the UI?
error handling
async

### completed component

    import { button } from "../elements/button";
    import { column } from "../elements/column";
    import { readJsonFile, showFilePicker } from "../utils/filesystem";
    import { parseProjectJson } from "../utils/parseProject";
    import { Component } from "./Component";
    import { Project } from "./Project";
    
    export class Start implements Component {
      #html: HTMLElement;
      constructor(setComponent: (component: Component) => void) {
        const newButton = button("new project", () => {
          setComponent(new Project());
        });
        const loadButtton = button("load json", async () => {
          try {
            const file = await showFilePicker();
            if (!file) {
              return;
            }
            const json = await readJsonFile(file);
            const project = parseProjectJson(json);
            setComponent(project);
          } catch (e) {
            console.error(e);
            alert("unable to load json");
          }
        });
        const menu = column([
          newButton,
          loadButtton,
        ])
        menu.classList.add("start")
        this.#html = menu
      }
    
      // --------------------------------------------------------------------------
      // Component Interface
      // --------------------------------------------------------------------------
      update(): void { }
      html(): HTMLElement {
        return this.#html;
      }
    }

style

    /* ------------------------- START -------------------------------- */
    .start {
      width: var(--width-start);
      margin: auto;
      margin-top: 3rem;
      gap: var(--padding-lg);
    }

## update app

set "Start" as the initial component instead of "Project"

    import { Component } from "./components/Component";
    import { Start } from "./components/Start";
    
    export class App {
      html: HTMLElement;
      component: Component;
    
      constructor() {
        this.component = new Start(this.setComponent.bind(this));
        this.html = this.component.html();
        setInterval(() => this.component.update(), 1000);
      }
      setComponent(component: Component) {
        this.component = component;
        this.html.replaceWith(this.component.html());
      }
    }

## done...?

Some small things that need to be done:

- prompt for filename when saving json file
- jsdoc on Components and Elements
- more tests? how can you test UI?
- custom design?
- limit currenly running task to 1
- multiple projects?
- save current state to local storage or cookies?
- anything else you want!

