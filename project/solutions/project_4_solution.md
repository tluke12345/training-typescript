# time-tracker - step 4 - Project Component

## overview

- 'Project' component
  - name
  - elapsedTime
  - add subtask button
  - edit button
    - edit mode
    - name text field
  - reset button
    - confirm popup
    - reset name

Todo:

- elapseTime value and formatting
- subtasks
- add subtask action
- save action

## Project

create 'src/components/Project.ts' file

create and export 'Project' class that implements the 'Component' interface

    import { text } from "../elements/text";
    import { Component } from "./Component";

    export class Project implements Component {
      // --------------------------------------------------------------------------
      // Component Interface
      // --------------------------------------------------------------------------
      update(): void {
        console.log("project updated")
      }
      html(): HTMLElement {
        return text("project")
      }
    }

We can now display this instead of the loading component. in 'src/app.ts',
replace loading component with this component

    import { Component } from "./components/Component";
    import { Project } from "./components/Project";
    
    export class App {
      html: HTMLElement;
      component: Component;
    
      constructor() {
        this.component = new Project();
        this.html = this.component.html();
        setInterval(() => this.component.update(), 1000);
      }
      setComponent(component: Component) {
        this.component = component;
        this.html.replaceWith(this.component.html());
      }
    }

add a private '#html' property. Return this property from Component 'html()'
method. Also, add a constructor and initialize it.

    import { text } from "../elements/text";
    import { Component } from "./Component";

    export class Project implements Component {

      #html: HTMLElement;

      constructor() {
        this.#html = text("project")
      }

      // --------------------------------------------------------------------------
      // Component Interface
      // --------------------------------------------------------------------------
      update(): void {
        console.log("project updated")
      }
      html(): HTMLElement {
        return this.#html
      }
    }

there are many buttons on this screen. add 'onAdd', 'onEdit', 'onReset', and 
'onSave' methods

    // --------------------------------------------------------------------------
    // Button Handlers
    // --------------------------------------------------------------------------
    onAdd() {}
    onEdit() {}
    onReset() {}
    onSave() {}

## UI

Lets define the basic components of this screen

- name element
- elapsed element
- subtask column
- add button -> calls 'onAdd'
- edit button -> calls 'onEdit'
- save button -> calls 'onSave'
- reset button -> calls 'onReset'

define these in the constructor and save as private properties

(careful about 'this')

    export class Project implements Component {
    
      #html: HTMLElement;
      #nameElement: HTMLElement;
      #elapsedElement: HTMLElement;
      #subtaskColumn: HTMLElement;
      #addButton: HTMLButtonElement;
      #editButton: HTMLButtonElement;
      #resetButton: HTMLButtonElement;
      #saveButton: HTMLButtonElement;
    
      constructor() {
        this.#nameElement = text("name")
        this.#elapsedElement = text("0:00")
        this.#subtaskColumn = column([])
        this.#addButton = button("+", this.onAdd.bind(this))
        this.#editButton = button("edit", this.onEdit.bind(this))
        this.#resetButton = button("reset", this.onReset.bind(this))
        this.#saveButton = button("save", this.onSave.bind(this))

        // TODO
        this.#html = text("project")
      }

    //..
    }

finally! layout the page. (set the #html property)

    constructor() {
      // ...

      const topRow = row([
        this.#nameElement,
        flexibleSpace(),
        this.#elapsedElement,
      ])

      const bottomRow = row([
        this.#editButton,
        flexibleSpace(),
        this.#saveButton,
        this.#resetButton,
      ])

      const page = column([
        topRow,
        this.#subtaskColumn,
        this.#addButton,
        flexibleSpace(),
        bottomRow,
      ]);

      this.#html = page;
    }

![Before styles](media/step_4_before_style.png)

## styles

Not very nice.. lets add some styles

- button
- button-circle (add button)
- button-red (reset)
- button-green (save)
- strong (project name and elapse text)
- project (page container)

like this

    /* ------------------------- BUTTON -------------------------------- */
    .button {
      background: var(--color-gray-bg);
      border: 1px solid var(--color-gray-border);
      border-radius: var(--border-radius-sm);
      cursor: pointer;
      display: flex;
      justify-content: center;
      align-items: center;
      height: var(--height-button);
      padding-left: var(--padding-md);
      padding-right: var(--padding-md);
    }
    
    .button:hover {
      background: var(--color-gray-hover);
    }
    
    .button-circle {
      width: var(--size-circle-button);
      height: var(--size-circle-button);
      align-self: center;
      border-radius: 50%;
      font-size: var(--font-size-strong);
    }
    
    .button-red {
      background: var(--color-red-bg);
      border: none;
    }
    
    .button-red:hover {
      background: var(--color-red-hover);
    }
    
    .button-green {
      background: var(--color-green-bg);
      border: none;
    }
    
    .button-green:hover {
      background: var(--color-green-hover);
    }
    
    /* ------------------------- TEXT -------------------------------- */
    .strong {
      font-size: var(--font-size-strong);
      font-weight: bold;
    }
    
    /* ------------------------- PROJECT -------------------------------- */
    .project {
      padding: var(--padding-lg);
      gap: var(--padding-md);
      max-width: var(--width-project);
      margin: auto;
    }
    
    .project-top-row,
    .project-subtask-column,
    .project-bottom-row {
      gap: var(--padding-sm);
    }

now we can add these classes to the components we made in the project
constructor

    constructor() {
      this.#nameElement = text("name")
      this.#nameElement.classList.add("strong")

      this.#elapsedElement = text("0:00")
      this.#elapsedElement.classList.add("strong")

      this.#subtaskColumn = column([])
      this.#subtaskColumn.classList.add("project-subtask-column")

      this.#addButton = button("+", this.onAdd.bind(this))
      this.#addButton.classList.add("button-circle")

      this.#editButton = button("edit", this.onAdd.bind(this))

      this.#resetButton = button("reset", this.onReset.bind(this))
      this.#resetButton.classList.add("button-red")

      this.#saveButton = button("save", this.onSave.bind(this))
      this.#saveButton.classList.add("button-green")

      const topRow = row([
        this.#nameElement,
        flexibleSpace(),
        this.#elapsedElement,
      ])
      topRow.classList.add("project-top-row")

      const bottomRow = row([
        this.#editButton,
        flexibleSpace(),
        this.#saveButton,
        this.#resetButton,
      ])
      bottomRow.classList.add("project-bottom-row")

      const page = column([
        topRow,
        this.#subtaskColumn,
        this.#addButton,
        flexibleSpace(),
        bottomRow,
      ]);
      page.classList.add("project")

      this.#html = page;
    }

Thats better

![After](media/step_4_after_style.png)

## Edit Mode

with an 'editing' mode, we want to hide/show UI elements sometimes. add a 
'hidden' utils css class with !important modifier

    /* ------------------------- UTILS -------------------------------- */
    .hidden {
      display: none !important;
    }

add a '#isEditing' private variable. in the 'onEdit', toggle this value.

edit mode -> hide 'save' and 'reset' buttons. change "edit" button title from 
'edit' to 'finish'

    export class Project implements Component {
      #isEditing: boolean = false;

      // ...

      onEdit() {
        this.#isEditing = !this.#isEditing;
        if (this.#isEditing) {
          this.#saveButton.classList.add("hidden")
          this.#resetButton.classList.add("hidden")
          this.#editButton.textContent = "finish"
        } else {
          this.#saveButton.classList.remove("hidden")
          this.#resetButton.classList.remove("hidden")
          this.#editButton.textContent = "edit"
        }
      }

      // ...
    }

Now, we need a text field to edit the name. add '#nameField'.

- visible only when editing
- onChange updates the value of 'nameElement'
- hide 'nameElement' when editing

like this

    // ...
    #nameField: HTMLInputElement;
    // ...

    constructor() {
      // ...
      this.#nameField = textField({
        placeholder: "project name",
        onChange: (value) => this.#nameElement.textContent = value ?? ''
      })
      this.#nameField.classList.add("hidden")
      // ...
    }

    // ...
    onEdit() {
      this.#isEditing = !this.#isEditing;
      if (this.#isEditing) {
        this.#saveButton.classList.add("hidden")
        this.#resetButton.classList.add("hidden")
        this.#editButton.textContent = "finish"
        this.#nameElement.classList.add("hidden")
        this.#nameField.classList.remove("hidden")
      } else {
        this.#saveButton.classList.remove("hidden")
        this.#resetButton.classList.remove("hidden")
        this.#editButton.textContent = "edit"
        this.#nameElement.classList.remove("hidden")
        this.#nameField.classList.add("hidden")
      }
    }
    // ...

now fix the 'input' styles

    /* ------------------------- INPUT -------------------------------- */
    input {
      padding: var(--padding-sm);
      border: 1px solid var(--color-gray-border);
      min-width: 50px;
      background: transparent;
    }

## reset

when the 'reset' button is pressed, show a confirmation dialog

    onReset() {
      const isConfirmed = confirm("Reset Project?");
      if (!isConfirmed) {
        return;
      }
    }

if the user confirms, reset the project name

    onReset() {
      const isConfirmed = confirm("Reset Project?");
      if (!isConfirmed) {
        return;
      }
      this.#nameElement.textContent = "Project"
      this.#nameField.value = ""
    }
