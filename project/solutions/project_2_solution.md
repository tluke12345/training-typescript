# time-tracker - step 2 - Elements

## overview

- button element
- column element
- row element
- text element
- textField element
- button element
- flexibleSpace element

## element creation step-by-step

Review the 'section 13 - dom intro'.

Create a file 'src/elements/button.ts'

Create and export a function named 'button' (use 'function' keyword)

    export function button() {}

Create a button using dom API and return it from the function

- title of "a button"
- class of "button"
- console.log("button clicked") when clicking on button

like this

    const b = document.createElement("button");
    b.textContent = "a button";
    b.classList.add("button");
    b.onclick = function() {
      console.log("button clicked");
    };
    return b;

Set the return type of the function

    export function button(): HTMLButtonElement {
        // ...
    }

Use the 'button' element in main.js. Add a button to the 'root' div.

    import { button } from "./elements/button";
    
    const root = document.getElementById("root");
    const b = button();
    root?.appendChild(b);

start vite server to see the button

    npm run dev

adjust the css styles (using javascript) to change the style

    b.style.color = "#000"
    b.style.padding = "5px 10px"

add a 'title' argument to the 'button' element. use it to set the button text

    export function button(title: string): HTMLButtonElement {
      const b = document.createElement("button");
      b.textContent = title;
      // ...
    }

we want to pass a function as an argument and set the 'onclick' to call that 
function. What is the type of a function that takes no arguments and returns
no values.

    () => void

add an 'onclick' argument to the 'button' element

    export function button(title: string, onClick: () => void): HTMLButtonElement {
      const b = document.createElement("button");
      b.textContent = title;
      b.onclick = onClick;
      // ..
    }

update the main.ts so it works again

    import { button } from "./elements/button";
    
    const root = document.getElementById("root");
    const b = button("title", () => {
      console.log("button pressed");
    });
    root?.appendChild(b);

remove styles (we don't need them here) and set 'button' css class

    export function button(title: string, onClick: () => void): HTMLButtonElement {
      const b = document.createElement("button");
      b.classList.add("button");
      b.textContent = title;
      b.onclick = onClick;
      return b;
    }

'button' element is done!

## Other elements

### column

- takes: 'children', a list of HTMLElements
- returns: an HTMLElement

use 'flexbox' to layout the children

    export function column(children: HTMLElement[]): HTMLElement {
      const e = document.createElement("div");
      e.style.display = "flex";
      e.style.flexDirection = "column";
      children.forEach((child) => e.appendChild(child));
      return e;
    }

### row

- takes: 'children', a list of HTMLElements
- returns: an HTMLElement

use 'flexbox' to layout the children

    export function row(children: HTMLElement[]): HTMLElement {
      const e = document.createElement("div");
      e.style.display = "flex";
      children.forEach((child) => e.appendChild(child));
      return e;
    }

### text

- takes: 'value', as string to display
- returns: an HTMLElement

like this

    export function text(value: string): HTMLElement {
      const e = document.createElement("div");
      e.textContent = value;
      return e;
    }

### flexibleSpacer

This is an object we can put into a row or column and will expand as much as
possible

- takes: no arguments
- return: an HTMLElement

use the 'flex' css property

    export function flexibleSpace(): HTMLElement {
      const e = document.createElement("div");
      e.style.flex = "2";
      return e;
    }

### textField (challenge)

- takes: 'placeholder', a string (optional)
- takes: 'initial', a string to set as the initial value (optional)
- takes: 'onChange', a function that is called
  - onChange takes 'value' an optional string and returns nothing
- returns: an HTMLInputElement

use 'object desctructuring' so that we can use named arguments

    export function textField({
      placeholder = undefined,
      initial = undefined,
      onChange,
    }: {
      placeholder?: string;
      initial?: string;
      onChange: (value?: string) => void;
    }): HTMLInputElement {
      const field = document.createElement("input");
      field.type = "text";
      if (placeholder !== undefined) {
        field.placeholder = placeholder;
      }
      if (initial !== undefined) {
        field.value = initial;
      }
      field.addEventListener("input", (e) => {
        const target = e.target as HTMLInputElement;
        const value = target.value;
        onChange(value);
      });
      return field;
    }

## Use the elements

in the 'main.ts' file, create the following html using the newly created
elements

- 'nav bar'
  - left: "Logo" text
  - right:
    - "product" button that prints 'go to product' when pressed
    - "blog" button that prints 'go to blog' when pressed
    - "store" button that prints 'go to store' when pressed
- 'content'
  - "name" text field with "name" placeholder. print to console when updated
  - "email" text field with "email" placeholder. print to console when updated

like this

    const el = column([
      // Header row
      row([
        text("Logo"),
        flexibleSpace(),
        button("products", () => console.log("go to products")),
        button("blog", () => console.log("go to blog")),
        button("store", () => console.log("go to store")),
      ]),
    
      // Content
      textField({
        placeholder: "name",
        onChange: (newName) => console.log("Set name to ", newName),
      }),
      textField({
        placeholder: "email",
        onChange: (newEmail) => console.log("Set email to ", newEmail),
      }),
    ])
    root?.appendChild(el);

or, a written differently (easier to configure!)

    // Navbar
    const navLogo = text("Logo");
    const navProduct = button("products", () => console.log("go to products"));
    const navBlog = button("blog", () => console.log("go to blog"));
    const navStore = button("store", () => console.log("go to store"));
    
    const navbar = row([navLogo, flexibleSpace(), navProduct, navBlog, navStore]);
    
    // Content Text Fields
    const nameField = textField({
      placeholder: "name",
      onChange: (newName) => console.log("Set name to ", newName),
    });
    const emailField = textField({
      placeholder: "email",
      onChange: (newEmail) => console.log("Set email to ", newEmail),
    });
    
    // Page Element
    const page = column([
      // Header row
      navbar,
    
      // Content
      nameField,
      emailField,
    ]);
    
    root?.appendChild(page);

![Step2 - before](media/step_2_before.png)

we can add a few styles and it is actually usuable now!

    // Navbar
    const navLogo = text("Logo");
    
    const navProduct = button("products", () => console.log("go to products"));
    navProduct.style.color = "black";
    navProduct.style.padding = "5px";
    
    const navBlog = button("blog", () => console.log("go to blog"));
    navBlog.style.color = "black";
    navProduct.style.padding = "5px";
    
    const navStore = button("store", () => console.log("go to store"));
    navStore.style.color = "black";
    navProduct.style.padding = "5px";
    
    const navbar = row([navLogo, flexibleSpace(), navProduct, navBlog, navStore]);
    navbar.style.gap = "1rem";
    
    // Content Text Fields
    const nameField = textField({
      placeholder: "name",
      onChange: (newName) => console.log("Set name to ", newName),
    });
    nameField.style.background = "transparent";
    nameField.style.padding = "5px";
    
    const emailField = textField({
      placeholder: "email",
      onChange: (newEmail) => console.log("Set email to ", newEmail),
    });
    emailField.style.background = "transparent";
    emailField.style.padding = "5px";
    
    // Page Element
    const page = column([
      // Header row
      navbar,
    
      // Content
      nameField,
      emailField,
    ]);
    page.style.gap = "1rem";
    page.style.padding = "1rem";
    
    root?.appendChild(page);

![Step2 - after](media/step_2_after.png)
