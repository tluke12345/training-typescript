# time-tracker - step 5 - Subtask Component

## overview

- Subtask
  - name
  - name field
  - start button
  - elapseTime
- styles
- edit mode
- running mode

TODO

- state
- elapseTime value and formatting

## General Steps

### Create Component

    import { text } from "../elements/text";
    import { Component } from "./Component";
    
    export class Subtask implements Component {
      #html: HTMLElement;
      constructor() {
        this.#html = text("subtask");
      }
    
      // --------------------------------------------------------------------------
      // Component Interface
      // --------------------------------------------------------------------------
      update(): void {
        console.log("subtask update");
      }
      html(): HTMLElement {
        return this.#html;
      }
    }

### button handlers

    // --------------------------------------------------------------------------
    // button handler
    // --------------------------------------------------------------------------
    onClickControl() {}

### Create elements

    #html: HTMLElement;

    #nameElement: HTMLElement;
    #controlButton: HTMLButtonElement;
    #elapseElement: HTMLElement;

    constructor() {
      this.#nameElement = text("Subtask");
      this.#controlElement = button("start", this.onClickControl.bind(this));
      this.#elapseElement = text("0:00");

      // TODO
      this.#html = text("subtask");
    }

### Create UI

    constructor() {
      this.#nameElement = text("Subtask");
      this.#controlButton = button("start", this.onClickControl.bind(this));
      this.#elapseElement = text("0:00");

      const subtask = row([
        this.#nameElement,
        flexibleSpace(),
        this.#controlButton,
        this.#elapseElement,
      ]);

      // TODO
      this.#html = subtask;
    }

### (temp) add a subtask to Project so we can see it

    // src/component/Project.constructor
    //..
    this.#subtaskColumn = column([
      new Subtask().html(),
    ])
    //..

### add styles

    // src/components/Subtask.constructor
    // ..
    this.#controlButton = button("start", this.onClickControl.bind(this));
    this.#controlButton.classList.add("button-green");
    // ..
    subtask.classList.add("subtask");
    // ..


    // public/css/styles.css
    /* ------------------------- SUBTASK -------------------------------- */
    .subtask {
      height: var(--height-subtask);
      align-items: center;
      border: 2px solid var(--color-gray-border);
      border-radius: var(--border-radius-lg);
      padding-left: var(--padding-sm);
      padding-right: var(--padding-sm);
      gap: var(--padding-sm);
    }
    
    .subtask-green {
      background: var(--color-green-tint);
      border: 2px solid var(--color-green-border);
    }
    
    .subtask-green .button-red {
      border: 2px solid white;
      border-radius: var(--border-radius-lg);
    }

### implement editing mode

    #nameField: HTMLInputElement;
    // ..
    constructor() {
      // ..
      this.#nameField = textField({
        placeholder: "subtask name",
        onChange: (value) => (this.#nameElement.textContent = value ?? ""),
      });
      // ..
      const subtask = row([
        this.#nameElement,
        this.#nameField,
        flexibleSpace(),
        this.#controlButton,
        this.#elapseElement,
      ]);
    }

    // --------------------------------------------------------------------------
    // State
    // --------------------------------------------------------------------------
    setEditing(isEditing: boolean) {
      if (isEditing) {
        this.#nameElement.classList.add("hidden");
        this.#nameField.classList.remove("hidden");
      } else {
        this.#nameElement.classList.remove("hidden");
        this.#nameField.classList.add("hidden");
      }
    }

### implement running mode

    isRunning: boolean = false;

    onClickControl() {
      if (this.isRunning) {
        this.#stop();
      } else {
        this.#start();
      }
    }

    #start() {
      this.isRunning = true;
      this.#html.classList.add("subtask-green");
      this.#controlButton.textContent = "stop";
      this.#controlButton.classList.add("button-red");
      this.#controlButton.classList.remove("button-green");
    }

    #stop() {
      this.isRunning = false;
      this.#html.classList.remove("subtask-green");
      this.#controlButton.textContent = "start";
      this.#controlButton.classList.remove("button-red");
      this.#controlButton.classList.add("button-green");
    }
