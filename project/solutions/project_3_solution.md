# time-tracker - step 3 - App

## overview

- Component interface
- App (display's a 'component')
- main (mount the app)
- Loading component

## App

the 'app' is the root of our code. The app is the container that is mounted to
the dom. everything happens within the app.

create 'src/app.ts'

create an 'App' class. export it so we can use it in different files

    export class App {}

expose an HTML property and initialize it in the class constructor

    export class App {
      html: HTMLElement;
    
      constructor() {
        // TODO: set this to the app content
        this.html = document.createElement("div")
      }
    }

## main

Now we have an 'App' we can attach it to the website's html

in main.ts, create an App instance and attach it's html to 'root' in index.html

    import { App } from "./app";
    
    const root = document.getElementById("root");
    const app = new App();
    root?.appendChild(app.html);

run the app and inspect the page. Your app's html should be inserted into the
website

## Component Interface

Now, back in 'app.ts', we want to set the content. But, we don't know what the
content will be yet... so we create an 'interface'.

We don't care what the 'content' is as long as we can get a HTMLElement from it
that we can return from our app.

Create an interface, 'src/components/Component.ts', with a single method,
'html()' that returns an HTMLElement

    export interface Component {
      html(): HTMLElement;
    }

## App's component property

Back in the "App.ts", we want to use this 'Component' interface.

in 'src/app.ts', add a property 'component' that conforms the the 'Component'
interface

    export class App {
      html: HTMLElement;
      component: Component;

    // ...
    }

add a 'setComponent' method so we can change this component later. make sure to
update the value of App.html as well. (note, this.html might already be added
to the DOM! you have to make sure the dom is updated as well!)

    setComponent(component: Component) {
      this.component = component;
      this.html.replaceWith(this.component.html())
    }

## create a 'Loading' component

my editor is complaining that "Property 'component' has no initializer and is
not definitely assigned in the constructor". We need to set the initial value
of 'component'. We will set it to a 'Loading' component

create 'src/components/Loading.ts' file

create and export a class 'Loading' that conforms to the 'Component' interface

    import { Component } from "./Component";
    
    export class Loading implements Component {}

now we have a message that 'Loading' doesn't conform to the 'Component'
interface. It doesn't have the 'html()' method! add it now. just return some
text that says 'loading'

## initialize 'component'

back in 'app.ts', in the constructor, set the initial value of 'component' to an
instanc of 'Loading'

    import { Component } from "./components/Component";
    import { Loading } from "./components/Loading";
    
    export class App {
      html: HTMLElement;
      component: Component;
    
      constructor() {
        this.component = new Loading()
        this.html = this.component.html()
      }
      setComponent(component: Component) {
        this.component = component;
        this.html.replaceWith(this.component.html())
      }
    }

Confirm that it works. when you render the website, it should now say loading

## 'update' method

Usually, you only add the functionality you need, when you need it.

But, because I have already created this app, I know we will need one more method
on the 'Component' interface. We can add it now to simplify things later.

This app is a 'clock' type app and therfore it will update every second. we will
add an 'update' method that we can call every second to have the component
update itself and its children.

to 'src/components/Component', add 'update()' method that returns nothing

    export interface Component {
      update(): void;
      html(): HTMLElement;
    }

Update 'src/components/Loading' with an 'update' method (that does nothing...)

    import { text } from "../elements/text";
    import { Component } from "./Component";
    
    export class Loading implements Component {
      html(): HTMLElement {
        return text("loading");
      }
      update() { }
    }

## setInterval

Now, in 'src/app.ts' where we have our component, we will also update it every
second.

Add a timer that calls the components 'update' method every second

    setInterval(() => this.component.update(), 1000);

Add a console.log to 'src/components/Loading.ts' update method so we can see
the interval happening

    update() {
      console.log("loading updated");
    }

reload the app in the browser and confirm the console logs

Good! we are now finally ready to actually add some UI to our app!
