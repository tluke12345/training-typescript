# time-tracker - step 8 - clock

## overview

## subtask state

Add 'startTimeStamp' which is a number (Date as milliseconds) if running,
undefined if not running

    startTimestamp: number | undefined;

Add 'intervals' which holds an array of the previous time durations that the
subtask was run

    intervals: TimeInterval[] = []

Handle 'start' and 'stop'

- Set 'startTime' when isRunning is set to true.
- Set save a new interval and set 'startTime' to undefined when isRunning is
  false

like this

    #start() {
      this.isRunning = true;
      this.startTimestamp = Date.now();
      this.#html.classList.add("subtask-green");
      this.#controlButton.textContent = "stop";
      this.#controlButton.classList.add("button-red");
      this.#controlButton.classList.remove("button-green");
    }

    #stop() {
      this.isRunning = false;
      this.intervals.push({
        startTime: this.startTimestamp,
        endTime: Date.now(),
      } as TimeInterval);
      this.startTimestamp = undefined;
      this.#html.classList.remove("subtask-green");
      this.#controlButton.textContent = "start";
      this.#controlButton.classList.remove("button-red");
      this.#controlButton.classList.add("button-green");
    }

## subtask elapseTime

create a method 'elapseTime' that returns the current elapse time for this
subtask.

    elapseTime(): number {
      // Running
      if (this.startTimestamp) {
        const allIntervals = [
          { startTime: this.startTimestamp, endTime: Date.now() },
          ...this.intervals,
        ];
        return calculateTotalElapseTime(allIntervals);
      }

      // Stopped
      return calculateTotalElapseTime(this.intervals);
    }

when subtask is set to running ('start' method), set UI to 'seconds' style
string

    #start() {
      this.isRunning = true;
      this.startTimestamp = Date.now();
      this.#html.classList.add("subtask-green");
      this.#controlButton.textContent = "stop";
      this.#controlButton.classList.add("button-red");
      this.#controlButton.classList.remove("button-green");

      const elapseString = convertElapseTimeToSecondsString(this.elapseTime());
      this.#elapseElement.textContent = elapseString;
    }

when subtask is set to stopped ('stop' method), set UI to 'minutes' style
string

    #stop() {
      this.isRunning = false;
      this.intervals.push({
        startTime: this.startTimestamp,
        endTime: Date.now(),
      } as TimeInterval);
      this.startTimestamp = undefined;
      this.#html.classList.remove("subtask-green");
      this.#controlButton.textContent = "start";
      this.#controlButton.classList.remove("button-red");
      this.#controlButton.classList.add("button-green");

      const elapseString = convertElapseTimeToMinuteString(this.elapseTime());
      this.#elapseElement.textContent = elapseString;
    }

when subtask is updated, we are running, so update the UI to 'seconds' style
string

    update(): void {
      const elapseString = convertElapseTimeToSecondsString(this.elapseTime());
      this.#elapseElement.textContent = elapseString;
    }

## project elapseTime

every time the project is 'updated', update the string displayed for the total
elapse time

    update(): void {
      // ..
      // Get total elapse time of ALL subtasks
      const totalElapseTime = this.#subtasks.reduce(
        (prev, subtask) => prev + subtask.elapseTime(),
        0,
      );
      const elapseString = convertElapseTimeToMinuteString(totalElapseTime);
      this.#elapsedElement.textContent = elapseString;
    }

## Test everything

Run the app. start multiple task:

- make sure "0:00:00" format while running
- make sure "0:00" format while stopped
- make sure elapse time value updates every second
- make sure the total elapse time is correct and updates (this will take a few
  minutes)
