# time-tracker - step 6 - Add Subtasks to Project

## overview

- Add subtasks to Project Component
  - add subtasks
  - edit subtasks
  - reset subtasks
  - update subtasks

## general steps

### subtasks array

    #subtasks: Subtask[] = [];

### add subtasks internal property

    onAdd() {
      const subtask = new Subtask(this.#isEditing)
      this.#addSubtask(subtask)
    }

    #addSubtask(subtask: Subtask) {
      this.#subtasks.push(subtask)
      this.#subtaskColumn.appendChild(subtask.html())
    }

### pass editing state to subtasks

    onEdit() {
      // ..
      this.#subtasks.forEach((subtask) => subtask.setEditing(this.#isEditing))
    }

### pass updates to subtasks

    update(): void {
      // If no running subtasks, no need to update
      const isRunning = this.#subtasks.some((subtask) => subtask.isRunning);
      if (!isRunning) {
        return;
      }

      // only update the running subtasks
      this.#subtasks
        .filter((subtask) => subtask.isRunning)
        .forEach((subtask) => subtask.update());

      // TODO: update Project total elapse time
    }

### reset subtasks

    onReset() {
      const isConfirmed = confirm("Reset Project?");
      if (!isConfirmed) {
        return;
      }
      this.#nameElement.textContent = "Project";
      this.#nameField.value = "";
      this.#subtasks = [];
      this.#subtaskColumn.innerHTML = "";
    }
