/**
 * Time Inteval
 */
export type TimeInterval = {
  /** intervals starting time in milliseconds */
  startTime: number;

  /** intervals ending time in milliseconds */
  endTime: number;
};
