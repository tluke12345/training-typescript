import { describe, it, expect } from "vitest";
import { TimeInterval } from "../models/TimeInterval";
import { calculateElapseTime, calculateTotalElapseTime, convertElapseTimeToMinuteString, convertElapseTimeToSecondsString } from "./timeUtils";

const i = (from: number, to: number) => {
  return {
    startTime: from,
    endTime: to,
  } as TimeInterval;
};

describe("timeUtils", () => {
  describe("calculateElapseTime", () => {
    [
      ["zero", i(0, 0), 0],
      ["postive range", i(1000, 2222), 1222],
      ["negative range", i(2000, 1111), 0],
      ["non-integer range, non-rounding", i(1000.1, 2000.2), 1000],
      ["non-integer range, rounding", i(1000.3, 2000.2), 999],
    ].forEach(([msg, testInterval, expected]) => {
      it(`has expected output for ${msg}`, () => {
        const elapse = calculateElapseTime(testInterval as TimeInterval);
        expect(elapse).toBe(expected);
      });
    });
  });

  describe("calculateTotalElapseTime", () => {
    [
      ["empty array", [], 0],
      ["single interval", [i(1000, 2222)], 1222],
      ["multiple intervals", [i(1000, 2222), i(4000, 4234)], 1456],
      ["handle zero value intervals", [i(1000, 2222), i(4500, 4234)], 1222],
    ].forEach(([msg, testIntervals, expected]) => {
      it(`has expected output for ${msg}`, () => {
        const elapse = calculateTotalElapseTime(testIntervals as TimeInterval[]);
        expect(elapse).toBe(expected);
      });
    });
  });

  describe("convertElapseTimetoSecondsString", () => {
    [
      ["zero", 0, "0:00:00"],
      ["minus value", -10000, "--:--:--"],
      ["seconds only", 10000, "0:00:10"],
      ["right before 2 minutes", 119000, "0:01:59"],
      ["just 2 minutes", 120000, "0:02:00"],
      ["right before 1 hour", 3599000, "0:59:59"],
      ["just 1 hour", 3600000, "1:00:00"],
      ["mix of hours and minutes", 36456000, "10:07:36"],
      ["100 hours or more", 360120000, "100:02:00"],
    ].forEach(([msg, interval, expected]) => {
      it(`has expected output for ${msg}`, () => {
        const actual = convertElapseTimeToSecondsString(interval as number);
        expect(actual).toBe(expected as string);
      });
    });
  });

  describe("convertElapseTimetoMinutesString", () => {
    [
      ["zero", 0, "0:00"],
      ["minus value", -10000, "--:--"],
      ["seconds only", 10000, "0:00"],
      ["right before 2 minutes", 119000, "0:01"],
      ["just 2 minutes", 120000, "0:02"],
      ["right before 1 hour", 3599000, "0:59"],
      ["just 1 hour", 3600000, "1:00"],
      ["mix of hours and minutes", 36456000, "10:07"],
      ["100 hours or more", 360120000, "100:02"],
    ].forEach(([msg, interval, expected]) => {
      it(`has expected output for ${msg}`, () => {
        const actual = convertElapseTimeToMinuteString(interval as number);
        expect(actual).toBe(expected as string);
      });
    });
  });
});
