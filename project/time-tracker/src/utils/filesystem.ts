// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function downloadJson(data: any, filename: string) {
  const file = new Blob([data], { type: "json" });
  const a = document.createElement("a");
  const url = URL.createObjectURL(file);
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 0);
}

export async function showFilePicker(): Promise<File | undefined> {
  return new Promise<File | undefined>((res, reject) => {
    const fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.onchange = () => {
      if (!fileInput.files || !fileInput.files[0]) {
        reject("Please select a file before clicking 'Load'");
        return;
      }
      res(fileInput.files[0]);
    };
    fileInput.click();
  });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function readJsonFile(file: File): Promise<any> {
  return new Promise((res, reject) => {
    const reader = new FileReader();
    reader.onload = (ev: ProgressEvent<FileReader>) => {
      try {
        const json = JSON.parse(ev.target!.result as string);
        res(json);
      } catch (e) {
        reject("Error parsing JSON");
      }
    };
    reader.onerror = () => {
      reject("Error reading file");
    };
    reader.readAsText(file);
  });
}
