import { Project } from "../components/Project";
import { Subtask } from "../components/Subtask";
import { TimeInterval } from "../models/TimeInterval";
import { isProjectData } from "./validation";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function parseProjectJson(json: any): Project {
  if (!isProjectData(json)) {
    throw new Error("invalid JSON format");
  }

  return new Project(
    json.name,
    json.subtasks.map(
      (subtaskData) =>
        new Subtask({
          name: subtaskData.name,
          intervals: subtaskData.intervals.map(
            (timeIntervalData) =>
              ({
                startTime: timeIntervalData.startTime,
                endTime: timeIntervalData.endTime,
              }) as TimeInterval,
          ),
        }),
    ),
  );
}
