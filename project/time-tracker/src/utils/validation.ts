// ----------------------------------------------------------------------------
// Data Interfaces
// ----------------------------------------------------------------------------
export interface TimeIntervalData {
  startTime: number;
  endTime: number;
}

export interface SubtaskData {
  name: string;
  intervals: TimeIntervalData[];
}

export interface ProjectData {
  name: string;
  subtasks: SubtaskData[];
}

// ----------------------------------------------------------------------------
// Type Guards
// ----------------------------------------------------------------------------
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isTimeIntervalData(obj: any): obj is TimeIntervalData {
  return (
    obj && typeof obj.startTime === "number" && typeof obj.endTime === "number"
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSubtaskData(obj: any): obj is SubtaskData {
  return (
    obj &&
    typeof obj.name === "string" &&
    Array.isArray(obj.intervals) &&
    obj.intervals.every(isTimeIntervalData)
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isProjectData(obj: any): obj is ProjectData {
  return (
    obj &&
    typeof obj.name === "string" &&
    Array.isArray(obj.subtasks) &&
    obj.subtasks.every(isSubtaskData)
  );
}
