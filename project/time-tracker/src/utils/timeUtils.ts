import { TimeInterval } from "../models/TimeInterval";

/**
 * determine the elapse time for a given TimeInterval
 *
 * @param { TimeInterval } interval whose elapse time will be calculated
 * @returns { number } elapse time in milliseconds, positive integer
 */
export function calculateElapseTime(interval: TimeInterval): number {
  const raw = interval.endTime - interval.startTime;
  const toPositive = Math.max(0, raw);
  const toInteger = Math.floor(toPositive);
  return toInteger;
}

/**
 * determine the total elapse time for all given TimeIntervals
 *
 * @param { TimeInterval[] } intervals whose total elapse time will be calculated
 * @returns { number } total elapse time in milliseconds, positive integer
 */
export function calculateTotalElapseTime(intervals: TimeInterval[]): number {
  return intervals
    .map((interval) => calculateElapseTime(interval))
    .reduce((prev, curr) => prev + curr, 0);
}

/**
 * Create a human readable string from elapseTime number
 *
 * example: 5515000 -> "1:31:55" (1 hour, 31 min, 55 sec)
 *
 * @param { number } elapseTime number of milliseconds to be converted
 * @return { string } human readable string
 */
export function convertElapseTimeToSecondsString(elapseTime: number): string {
  if (elapseTime < 0) {
    return "--:--:--";
  }
  try {
    const totalSeconds = Math.floor(elapseTime / 1000);
    const seconds = totalSeconds % 60;
    const secondsStr = seconds.toFixed(0).padStart(2, "0");
    const totalMinutes = Math.floor(totalSeconds / 60);
    const minutes = totalMinutes % 60;
    const mintessStr = minutes.toFixed(0).padStart(2, "0");
    const hours = Math.floor(totalMinutes / 60);
    return `${hours}:${mintessStr}:${secondsStr}`;
  } catch (e) {
    console.error("error converting elapsed time into display string: ", e);
    return "--:--:--";
  }
}

/**
 * Create a human readable string from elapseTime number
 *
 * example: 5515000 -> "1:31" (1 hour, 31 min)
 *
 * @param { number } elapseTime number of milliseconds to be converted
 * @return { string } human readable string
 */
export function convertElapseTimeToMinuteString(
  elapseTime: number,
): string {
  if (elapseTime < 0) {
    return "--:--";
  }
  try {
    const totalMinutes = Math.floor(elapseTime / 1000 / 60);
    const minutes = totalMinutes % 60;
    const mintessStr = minutes.toFixed(0).padStart(2, "0");
    const hours = Math.floor(totalMinutes / 60);
    return `${hours}:${mintessStr}`;
  } catch (e) {
    console.error("error converting elapsed time into display string: ", e);
    return "--:--";
  }
}
