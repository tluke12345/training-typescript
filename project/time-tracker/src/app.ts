import { Component } from "./components/Component";
import { Start } from "./components/Start";

export class App {
  html: HTMLElement;
  component: Component;

  constructor() {
    this.component = new Start(this.setComponent.bind(this));
    this.html = this.component.html();
    setInterval(() => this.component.update(), 1000);
  }
  setComponent(component: Component) {
    this.component = component;
    this.html.replaceWith(this.component.html());
  }
}
