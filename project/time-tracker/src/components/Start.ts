import { button } from "../elements/button";
import { column } from "../elements/column";
import { readJsonFile, showFilePicker } from "../utils/filesystem";
import { parseProjectJson } from "../utils/parseProject";
import { Component } from "./Component";
import { Project } from "./Project";

export class Start implements Component {
  #html: HTMLElement;
  constructor(setComponent: (component: Component) => void) {
    const newButton = button("new project", () => {
      setComponent(new Project());
    });
    const loadButtton = button("load json", async () => {
      try {
        const file = await showFilePicker();
        if (!file) {
          return;
        }
        const json = await readJsonFile(file);
        const project = parseProjectJson(json);
        setComponent(project);
      } catch (e) {
        console.error(e);
        alert("unable to load json");
      }
    });
    const menu = column([
      newButton,
      loadButtton,
    ])
    menu.classList.add("start")
    this.#html = menu
  }

  // --------------------------------------------------------------------------
  // Component Interface
  // --------------------------------------------------------------------------
  update(): void { }
  html(): HTMLElement {
    return this.#html;
  }
}
