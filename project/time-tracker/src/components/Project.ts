import { button } from "../elements/button";
import { column } from "../elements/column";
import { flexibleSpace } from "../elements/flexibleSpace";
import { row } from "../elements/row";
import { text } from "../elements/text";
import { textField } from "../elements/textField";
import { downloadJson } from "../utils/filesystem";
import { convertElapseTimeToMinuteString } from "../utils/timeUtils";
import { Component } from "./Component";
import { Subtask } from "./Subtask";

export class Project implements Component {
  #subtasks: Subtask[];
  #isEditing: boolean = false;

  #html: HTMLElement;
  #nameElement: HTMLElement;
  #nameField: HTMLInputElement;
  #elapsedElement: HTMLElement;
  #subtaskColumn: HTMLElement;
  #addButton: HTMLButtonElement;
  #editButton: HTMLButtonElement;
  #resetButton: HTMLButtonElement;
  #saveButton: HTMLButtonElement;

  constructor(name?: string, subtasks?: Subtask[]) {
    this.#subtasks = subtasks ?? [];
    this.#nameElement = text(name ?? "name");
    this.#nameElement.classList.add("strong");

    this.#nameField = textField({
      placeholder: "project name",
      initial: name,
      onChange: (value) => (this.#nameElement.textContent = value ?? ""),
    });
    this.#nameField.classList.add("hidden");

    this.#elapsedElement = text("0:00");
    this.#elapsedElement.classList.add("strong");

    this.#subtaskColumn = column(
      (subtasks ?? []).map((subtask) => subtask.html()),
    );
    this.#subtaskColumn.classList.add("project-subtask-column");

    this.#addButton = button("+", this.onAdd.bind(this));
    this.#addButton.classList.add("button-circle");

    this.#editButton = button("edit", this.onEdit.bind(this));

    this.#resetButton = button("reset", this.onReset.bind(this));
    this.#resetButton.classList.add("button-red");

    this.#saveButton = button("save", this.onSave.bind(this));
    this.#saveButton.classList.add("button-green");

    const topRow = row([
      this.#nameElement,
      this.#nameField,
      flexibleSpace(),
      this.#elapsedElement,
    ]);
    topRow.classList.add("project-top-row");

    const bottomRow = row([
      this.#editButton,
      flexibleSpace(),
      this.#saveButton,
      this.#resetButton,
    ]);
    bottomRow.classList.add("project-bottom-row");

    const page = column([
      topRow,
      this.#subtaskColumn,
      this.#addButton,
      flexibleSpace(),
      bottomRow,
    ]);
    page.classList.add("project");

    this.#html = page;
  }

  // --------------------------------------------------------------------------
  // Button Handlers
  // --------------------------------------------------------------------------
  onAdd() {
    const subtask = new Subtask({ isEditing: this.#isEditing });
    this.#addSubtask(subtask);
  }

  onEdit() {
    this.#isEditing = !this.#isEditing;
    if (this.#isEditing) {
      this.#editButton.textContent = "finish";
      this.#saveButton.classList.add("hidden");
      this.#resetButton.classList.add("hidden");
      this.#nameElement.classList.add("hidden");
      this.#nameField.classList.remove("hidden");
    } else {
      this.#editButton.textContent = "edit";
      this.#saveButton.classList.remove("hidden");
      this.#resetButton.classList.remove("hidden");
      this.#nameElement.classList.remove("hidden");
      this.#nameField.classList.add("hidden");
    }
    this.#subtasks.forEach((subtask) => subtask.setEditing(this.#isEditing));
  }

  onReset() {
    const isConfirmed = confirm("Reset Project?");
    if (!isConfirmed) {
      return;
    }
    this.#nameElement.textContent = "Project";
    this.#nameField.value = "";
    this.#subtasks = [];
    this.#subtaskColumn.innerHTML = "";
  }

  onSave() {
    const json = JSON.stringify(this.data());
    const filename = `${this.#nameElement.textContent} - ${new Date()}.json`;
    downloadJson(json, filename);
  }

  // --------------------------------------------------------------------------
  // State
  // --------------------------------------------------------------------------
  #addSubtask(subtask: Subtask) {
    this.#subtasks.push(subtask);
    this.#subtaskColumn.appendChild(subtask.html());
  }

  data() {
    return {
      name: this.#nameField.value,
      subtasks: this.#subtasks.map((subtask) => subtask.data()),
    };
  }

  // --------------------------------------------------------------------------
  // Component Interface
  // --------------------------------------------------------------------------
  update(): void {
    // If no running subtasks, no need to update
    const isRunning = this.#subtasks.some((subtask) => subtask.isRunning);
    if (!isRunning) {
      return;
    }

    // only update the running subtasks
    this.#subtasks
      .filter((subtask) => subtask.isRunning)
      .forEach((subtask) => subtask.update());

    // Get total elapse time of ALL subtasks
    const totalElapseTime = this.#subtasks.reduce(
      (prev, subtask) => prev + subtask.elapseTime(),
      0,
    );
    const elapseString = convertElapseTimeToMinuteString(totalElapseTime);
    this.#elapsedElement.textContent = elapseString;
  }
  html(): HTMLElement {
    return this.#html;
  }
}
