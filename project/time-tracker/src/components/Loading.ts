import { text } from "../elements/text";
import { Component } from "./Component";

export class Loading implements Component {
  html(): HTMLElement {
    return text("loading");
  }
  update() {
    console.log("loading updated");
  }
}
