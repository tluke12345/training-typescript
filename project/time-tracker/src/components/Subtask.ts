import { button } from "../elements/button";
import { flexibleSpace } from "../elements/flexibleSpace";
import { row } from "../elements/row";
import { text } from "../elements/text";
import { textField } from "../elements/textField";
import { TimeInterval } from "../models/TimeInterval";
import {
  calculateTotalElapseTime,
  convertElapseTimeToMinuteString,
  convertElapseTimeToSecondsString,
} from "../utils/timeUtils";
import { Component } from "./Component";

export class Subtask implements Component {
  isRunning: boolean = false;
  startTime: number | undefined;
  intervals: TimeInterval[];

  #html: HTMLElement;

  #nameElement: HTMLElement;
  #nameField: HTMLInputElement;
  #controlButton: HTMLButtonElement;
  #elapseElement: HTMLElement;

  constructor({
    isEditing = false,
    name,
    intervals,
  }: {
    isEditing?: boolean;
    name?: string;
    intervals?: TimeInterval[];
  } = {}) {
    this.intervals = intervals ?? [];
    this.#nameElement = text(name ?? "Subtask");
    this.#nameField = textField({
      placeholder: "subtask name",
      initial: name,
      onChange: (value) => (this.#nameElement.textContent = value ?? ""),
    });
    this.#nameField.classList.add("hidden");
    this.#controlButton = button("start", this.onClickControl.bind(this));
    this.#controlButton.classList.add("button-green");
    this.#elapseElement = text("0:00");
    if (intervals !== undefined && intervals.length > 0) {
      const elapsedString = convertElapseTimeToMinuteString(this.elapseTime());
      this.#elapseElement.textContent = elapsedString;
    }

    const subtask = row([
      this.#nameElement,
      this.#nameField,
      flexibleSpace(),
      this.#controlButton,
      this.#elapseElement,
    ]);
    subtask.classList.add("subtask");

    this.#html = subtask;

    this.setEditing(isEditing);
  }

  // --------------------------------------------------------------------------
  // button handler
  // --------------------------------------------------------------------------
  onClickControl() {
    if (this.isRunning) {
      this.#stop();
    } else {
      this.#start();
    }
  }

  // --------------------------------------------------------------------------
  // State
  // --------------------------------------------------------------------------
  setEditing(isEditing: boolean) {
    if (isEditing) {
      this.#nameElement.classList.add("hidden");
      this.#nameField.classList.remove("hidden");
    } else {
      this.#nameElement.classList.remove("hidden");
      this.#nameField.classList.add("hidden");
    }
  }

  elapseTime(): number {
    // Running
    if (this.startTime) {
      const allIntervals = [
        { startTime: this.startTime, endTime: Date.now() },
        ...this.intervals,
      ];
      return calculateTotalElapseTime(allIntervals);
    }

    // Stopped
    return calculateTotalElapseTime(this.intervals);
  }

  data() {
    return {
      name: this.#nameField.value,
      startTime: this.startTime,
      intervals: this.intervals,
    };
  }

  #start() {
    this.isRunning = true;
    this.startTime = Date.now();
    this.#html.classList.add("subtask-green");
    this.#controlButton.textContent = "stop";
    this.#controlButton.classList.add("button-red");
    this.#controlButton.classList.remove("button-green");

    const elapseString = convertElapseTimeToSecondsString(this.elapseTime());
    this.#elapseElement.textContent = elapseString;
  }

  #stop() {
    this.isRunning = false;
    this.intervals.push({
      startTime: this.startTime,
      endTime: Date.now(),
    } as TimeInterval);
    this.startTime = undefined;
    this.#html.classList.remove("subtask-green");
    this.#controlButton.textContent = "start";
    this.#controlButton.classList.remove("button-red");
    this.#controlButton.classList.add("button-green");

    const elapseString = convertElapseTimeToMinuteString(this.elapseTime());
    this.#elapseElement.textContent = elapseString;
  }

  // --------------------------------------------------------------------------
  // Component Interface
  // --------------------------------------------------------------------------
  update(): void {
    const elapseString = convertElapseTimeToSecondsString(this.elapseTime());
    this.#elapseElement.textContent = elapseString;
  }
  html(): HTMLElement {
    return this.#html;
  }
}
