export interface Component {
  update(): void;
  html(): HTMLElement;
}
