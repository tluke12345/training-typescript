export function text(value: string): HTMLElement {
  const e = document.createElement("div");
  e.textContent = value;
  return e;
}
