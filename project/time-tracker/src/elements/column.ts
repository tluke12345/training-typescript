export function column(children: HTMLElement[]): HTMLElement {
  const e = document.createElement("div");
  e.style.display = "flex";
  e.style.flexDirection = "column";
  children.forEach((child) => e.appendChild(child));
  return e;
}
