export function row(children: HTMLElement[]): HTMLElement {
  const e = document.createElement("div");
  e.style.display = "flex";
  children.forEach((child) => e.appendChild(child));
  return e;
}
