export function textField({
  placeholder = undefined,
  initial = undefined,
  onChange,
}: {
  placeholder?: string;
  initial?: string;
  onChange: (value?: string) => void;
}): HTMLInputElement {
  const field = document.createElement("input");
  field.type = "text";
  if (placeholder !== undefined) {
    field.placeholder = placeholder;
  }
  if (initial !== undefined) {
    field.value = initial;
  }
  field.addEventListener("input", (e) => {
    const target = e.target as HTMLInputElement;
    const value = target.value;
    onChange(value);
  });
  return field;
}
