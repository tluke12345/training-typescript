export function button(title: string, onClick: () => void): HTMLButtonElement {
  const b = document.createElement("button");
  b.classList.add("button");
  b.textContent = title;
  b.onclick = onClick;
  return b;
}
