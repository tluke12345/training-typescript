export function flexibleSpace(): HTMLElement {
  const e = document.createElement("div");
  e.style.flex = "2";
  return e;
}
