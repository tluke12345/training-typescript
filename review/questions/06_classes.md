# classes

create a class for a user

notes about classes

- class names should be Caplitalized, camel-case -> 'TimeInterval'
- your editor might give you a warning if you have more than one class per file

export the class so we can use it in other files!

use this class. create an istance called 'user1'

## properties

add two properties to the 'User' class: name and age

set 'age' as optional

set a default value for 'name' to be 'john'

finally, the the errors are gone an we can use the class. create 'user1', an
instance of this class.

- what is the value of 'name' and 'age'.
- change name to '田中'
- change age to 102

add a 'hidden' property 'id' that defaults to "12345".

- How can we make a hidden attribute in typescript?
- what about javascript?
- what is the different between typescript and javascripts hidden property?
- what is the "'id' is declared but its value is never read" error?
- try and access the 'id'
- try and set the 'id'

## constructor

add a constructor to the User class (no arguments)

the constructor is just a function. refer to 05_functions.

add a 'name' argument to the constructor

add an optional 'age' argument

the 'id' is still giving a warning because it is never used! log it's value to
the console in the constructor to get rid of the warning.

remove the 'id' default value. instead, set it to a 10 char string generated
from a random number! (you have to do this in the constructor...)

set the 'name' and 'age' class properties to the values passed into the
constructor

set the class property 'age' to 12 if no value is passed into the constructor.

change the constructor to use 'deconstructed object'

## methods

methods are (basically) just functions attached to a class.

add a 'printId' method to User

call this method on your 'user1' object

add a 'incrementAgeBy' method. takes 1 number argument. the 'age' property
should be increased by that number amount

call this method on your 'user1' object

add a 'getId' method.

call this method on your 'user1' object

add a private method with javascript. how about typescript.

## this

'this' is confusing. it can cause a lot of problems. type the following code
into your editor and run it...

    class A {
      name = "class A"
      printName() {
        console.log('this.name ->', this.name);
      }
    }

    class B {
      callback: () => void;
      constructor(callback: () => void) {
        this.callback = callback;
      }
      go() {
        this.callback()
      }
    }

    const a = new A()
    const b = new B(a.printName)

    b.go();

what happens? why!?

fix with 'bind'

fix with arrow function

## inheritance

Just don't do it... (there may be a couple good use-cases... but I can't think
of any... 'events' or 'errors' maybe...)
