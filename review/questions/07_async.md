# async

## callbacks

"Event" pattern. the 'old' way of async code. When are why is it still useful?

create a fake 'Button' class. expose a 'onClick' callback method. Use the class.

Use the 'Button' class.

create a fake 'accessAPI' function. It takes an 'onFinished' callback.

Use the 'accessAPI' method

## promises

'Newer' style of async code.

a 'async' function returns imediately with a 'Promise' object. this object is
initially 'empty', then when the operation is complete, the object value can be
read.

Create a promise that:

- takes 3 seconds to finish.
- creates a random number from 1~10
- returns the number if greater than 5
- otherwise throws an error that says "too small"

### .then and .catch

use '.then' and '.catch' to handle the promise

### async / await

create an 'async' function that handles the promise using 'await'. how do you
handle the error?

### convert Callbacks -> Promises (so we can use async / await) (challenge)

Convert XMLHttpRequest get call to a promise so we can use async / await

using the callback api

convert to promise

Use the promisw
