# Types

What is a type?

| value                          | type     |
| ------------------------------ | -------- |
| 1                              | number   |
| 1.433                          | number   |
| "hello"                        | string   |
| true                           | boolean  |
| [1,2,3]                        | array    |
| { name: "john", age: 21}       | object   |
| (a:number) => {console.log(a)} | function |

## numbers

define a number with value 20. what is the type?

define a number with value 22.22. what is the type?

define a number with value 2.555e10. what is type?

what is maximium and minimum values of a number?

what is maximum and minimum INTEGER values of number?

what is the difference between the max/min value and max/min integer?

## special types

what is the 'any' type

what is the 'unknown' type

when do you use 'any'

when do you use 'unknown'

what is undefined? what is null? when do you use them?

## enum

create a _numeric_ enum 'Direction' with 4 cardinal directions

create a _numberic_ enum 'StatusCode' with some http status codes

create a _string_ enum 'Color' with red, green, and blue

what is the difference between a _numberic_ and _string_ enum

## functions

type of this function: function() {console.log("hello")}

type of this function: function(x: string) { return parseInt(x) }

type of this function: function(s: string, n: number, b: boolean) { const a = n }

type of this function: function() { throw new Error("problem") }

type of this function: function() { while(true) {console.log("infinite") } }

type of this function: () => {console.log("hello") }

## checking types

check if variable 'x' is a string

check if variable 'x' is an integer

check if 'error' is of type 'DTPError'

### typeof vs instanceof

|             | typeof                 | instanceof            |
| ----------- | ---------------------- | --------------------- |
| when to use | primative values       | classes               |
| output      | type as a string       | boolean               |
| usage       | check simple primative | check complex objects |

## coercing types

make typescript think 'x' is a number

make typescript think 'y' is object with 'name' property

use <> notation to make typescript thing 'z' is either a boolean or undefined

## type alias

create a type 'StringOrNumber'

create a type 'Employee' with 'name' and 'department'

create a type 'Manger' with 'name' and 'teamSize'

create a type DeparmentManager that has all the attributes of both 'Employee'
and 'Manager'

create a type for 'Account' that has required 'id' and optional 'balance'

create a type for 'User' with 'name', 'password', and 'email'. 'email' can be
undefined!

create a type for an array that has only numbers

create at type for an array that has numbers OR strings

create a type for an array that holds any type

## interface

create interface for a 'Vehicle' that can 'accelerate' and 'applyBrakes'

create interface for 'Clock' with 'currentTime' and optional 'changeBatteries'
methods

apply 'Vehicle' interface to 'Car' class

apply 'Clock' interface to 'DigitalClock'

apply 'Clock' interface to 'AnalogClock' (this clock doesn't have batteries)

### interface vs type alias

an interface and type alias are almost the same thing. They can usually be used
interchangeably. They can both specify properties and methods of an object.

My recommendation (guideline not a rule!):

- type alias: properties only -> describe the 'shape' of the data
- interface: methods only -> describe the 'abilities' of object
