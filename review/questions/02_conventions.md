# Typescript conventions

- use 'const' (_sometimes_ 'let' is ok, _NEVER_ use var)
- use '===' ('==' checks 'falsy' or 'truthy' which is unreliable)
- ';' are optional (I don't bother adding... 'formatter' adds for me!)

## naming

Variables: not capitalized, camel-case

```typescript
// BAD
const Hello
const hello-world
const hello_world
const 1helloWorld

// GOOD
const hello
const helloWorld
const helloWorld1
```

Functions: not capitalized, camel-case

```typescript
// BAD
function FindMatches() {}
function find-matches() {}
function find_matches() {}
function 1findMatches() {}

// GOOD
function findMatches() {}
```

Classes, react components: capitalized, camel-case

```typescript
// BAD
class car {}
class Three-wheeled-bike {}
class Three_Wheeled_Bike {}

// GOOD
class Car {}
class ThreeWheeledBike {}

// React
function InputComponent() {
    return <div></div>
}
```

Filenames: depends on project! consistency is important!

```text
// function / util file
utils.ts
convertAccount.ts

// Class / component file
UserModel.ts
Home.tsx

// some projects
convert-account.ts
```
