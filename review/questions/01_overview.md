# Overview

## Javascript

When we say 'typescript' we are actually talking about javascript. Browsers and
servers ONLY understand javascript and will give an error if you try and run
typescript.

typescript is converted to javascript before we actually use it.

So:

- understanding javascript is just as (or more) important than typescript
- tools we use to solve problems (time, math, crypo, random, etc...) are all
  javascript

Javascript is really weird:

```javascript
"b" + "a" + +"a" + "a"; // -> 'baNaNa'
"foo" + +"bar"; // -> 'fooNaN'
NaN === NaN; // -> false

(![] + [])[+[]] +
  (![] + [])[+!+[]] +
  ([![]] + [][[]])[+!+[] + [+[]]] +
  (![] + [])[!+[] + !+[]];
// -> 'fail'

!!null; // -> false
null == false; // -> false
0 == false; // -> true
"" == false; // -> true

typeof NaN; // -> 'number'

0.1 + 0.2; // -> 0.30000000000000004
0.1 + 0.2 === 0.3; // -> false

... // many many more
```

## Typescript

What is typescript?

> Typescript -> javascript + type information

for example:

```typescript

// Typescript
function chargePhone(current: number, amount: number): number {
  const console.log(`current charge: ${current}%`);
  const chargedAmount = current + amount;
  const console.log(`after charging: ${chargedAmount}%`);
  return 100 - chargedAmount;
}

// equivalent javascript
function chargePhone(current, amount) {
  const console.log(`current charge: ${current}%`);
  const chargedAmount = current + amount;
  const console.log(`after charging: ${chargedAmount}%`);
  return 100 - chargedAmount;
}
```

The only thing added was 'number' to indicate the _type_ of the arguments and
return value.

Other things we can use thanks to typescript:

- enums
- interfaces
- type definitions
- generics
- namespaces and modules
- type guarding
- decorators
- etc...

### Why use typescript

Typescript is hard. Why use it?

If you want the features listed above you need typescript. But, we don't _need_
those features!

There are two reasons why typescript is good:

- type checking / safety
- autocomplete / errors

With _type checking_ VSCode (actually, the typescript language server) will
make sure that you only use values in the correct way. You cannot use a number
when you are supposed to use a boolean. The program will not work until you fix
it.

With _autocomplete / errors_ VSCode (this is also the LSP and other linters)
will help you choose the appropriate value and give you warning and error
messages that describe why something is wrong. These are sometimes useful!

## browser and nodejs

Javascript (and therefore typescript) can be used in two places: the browser
and the server with nodejs.

### browser

Javascript was originally created for the browser (1995). the first prototype
was created in just 10 days. (thats probably why javascript has so many problems)

Today, ALL browsers use javascript (NO browsers understand typescript...).

Some things specific to the browser:

- react (usually)
- bundling tools (vite / webpack / parcel / etc)
- dom (document object model - how we interact with a website from javascript)

### nodejs

Much later (2009), nodejs was created. using nodejs, we can run javascript on
a computer without a browser. People started thinking "I can write server and
client in the same language!".

Some things about nodejs:

- express
- ts-node (run typescript directly without compiling to javascript)
- nodemon (automatically re-compile, restart on file changes)
- tools to access computer (filesystem / network / etc.)

