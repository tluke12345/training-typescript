# Useful tools

## "..."

What happens when you add two arrays: [1,2,3] + [4,5,6]

We WANT '[1,2,3,4,5,6]' (concatenate). concatenate with for loops

concatenate with 'concat' method

concatenate with '...' operator

What happens when you add two objects: {name: "john"} + {age: 32}

We WANT '{name: "john", age: 32}' (combination). combine with for loops

combine with 'Object.assign()'

combine with '...'

Create a copy of object a

create copy of array b

create copy of 'account' object, update the 'balance' to be 3444

## ??, ||

- ?? -> undefined or null only
- || -> any 'falsy' value

set 'price' equal to 'salePrice'. if 'salePrice' is 'undefined' set 'price' to 10

set 'price' equal to 'salePrice'. if 'salePrice' is 'null' set 'price' to 10

set 'price' equal to 'salePrice'. if 'salePrice' is '0' set 'price' to 10

set 'price' equal to 'salePrice'. if 'salePrice' is 'NaN' set 'price' to 10

set 'price' equal to 'salePrice'. if 'salePrice' is '[]' set 'price' to 10

set 'price' equal to 'salePrice'. if 'salePrice' is 'false' set 'price' to 10

set 'price' equal to 'salePrice'. if 'salePrice' is "" set 'price' to 10

## &&

render react component 'UserCard' only if variable 'user' has been set

render react component 'UserCard' only if 'user' is set, no 'error', and
'isLoggedIn' is true

## A ? B : C

write the following on one line using '?:'

    // QUESTION
    if (x === 3) {
        console.log(x - 3)
    } else {
        console.log(x)
    }

render 'UserCard' if 'isLoading' is false, otherwise render 'LoadingIndicator'

## formatted string

print "hello USER, it is TIME now", USER and TIME are variables (use '+' only!)

print "hello USER, it is TIME now", USER and TIME are variables (formatted string)
