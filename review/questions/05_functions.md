# functions

for the rest of the file:

    type User = {
        name: string;
        email: string;
    }

## define

create a function named 'printTime'. print the current time to console

call the function

what is the return type? explicitly add it

## return value type

create a function named 'getCurrentHour'. it should return the current hour as
a number

How do we know what the return type is? how can we use VSCode to learn the
return type?

explicitly define the return type

create a type 'User' and return a user or undefined from the 'getUser' function

create a function that returns a number or a string or an array of strings or
nothing (undefined). why is this helpfu? why is it harmful?

what error will this give us?: `async function a(): number { //.. }`

consider the following

- explicit: `function a(): number { return 1 }`
- implicit: `function a() { return 1 }`

when and why would we explicitly specify the return type?

## args

write function 'a' that takes zero arguments

write function 'b' that takes one string argument

write function 'c' that takes 2 arguments, a string and a number.

write function 'd' that takes 1 argument which is a string or number.

write a function 'setUser' that takes a 'User' argument

- using the 'User' type
- without using the 'User' type (define user inline)

## default value

create function 'b' with one string argument, default value is "foo"

create function 'c' with 2 arguments, string (default "foo"), number (default 1)

try writing this: `function d(str: string = "foo", num: number) {}`. does it
work? try calling it with only a number (and using the default str value)

create a function that takes a 'User' object. set the default user to {}

## optional value

write a function with an optional string argument

so, the '?' is the same as TYPE | undefined

does this function work: `function b(val1?: string, val2: string) {}`

does this function work: `function c(val1: string | undefined, val2: string) {}`

## destructured object (!important)

create function 'a' that has 1 argument, a 'User' object

use this function

same as above, but don't use 'User', define inline

now 'destructure' the user so we can use 'name' and 'emails' directly!

set 'name' with DEFAULT value of "name"

what happens when we try to use the default name `a({email: "travis@gmail.com"})`

set 'name' with default, make 'name' as optional

## '...rest'

this is sometimes used in react so lets review.

what is this: `[...a, ...b]`

what is this: `const {a, b, ...z} = {a:1, b:2, c:3, d:4, e:5}`

these both use the '...' but, this is actually two different operators!

sometimes you will see this in react:

`<BankAccount key={index} {...account} \>`

what is happening the the 'account' variable

## arrow functions

convert to arrow function `function a() { //.. }`

convert to arrow function `function b(arg1: string) { //.. }`

convert to arrow function `function c(arg1: string): number { //.. }`

convert to arrow function `async function d(arg1: number): Promise<void> { //.. }`
