# Useful tools

## '...'

What happens when you add two arrays: [1,2,3] + [4,5,6]

    - converts to strings then string concatenation: "1,2,34,5,6"

We WANT '[1,2,3,4,5,6]' (concatenate). concatenate with for loops

    const a = [1,2,3]
    const b = [4,5,6]
    const result: number[] = []

    for (let i = 0; i < a.length; i++) {
        result.push(a[i]);
    }
    for (let j = 0; j < b.length; j++) {
        result.push(b[j]);
    }
    console.log(result);  // Outputs: [1, 2, 3, 4, 5, 6]

concatenate with 'concat' method

    const a = [1,2,3]
    const b = [4,5,6]
    const result = a.concat(b)
    console.log(result);  // Outputs: [1, 2, 3, 4, 5, 6]

concatenate with '...' operator

    const a = [1,2,3]
    const b = [4,5,6]
    const result = [...a, ...b]
    console.log(result);  // Outputs: [1, 2, 3, 4, 5, 6]

What happens when you add two objects: {name: "john"} + {age: 32}

    - converts to string then string concatenation: "[object Object][object Object]"

We WANT '{name: "john", age: 32}' (combination). combine with for loops

    const a = {name: "john"}
    const b = {age: 32}
    result = {[key: string]: any} = {}

    for (const key in a) {
        if (a.hasOwnProperty(key)) {
            result[key] = a[key]
        
        }
    }
    for (const key in b) {
        if (b.hasOwnProperty(key)) {
            result[key] = b[key]
        
        }
    }
    console.log(result);  // Outputs: {name: "john", age: 32}

combine with 'Object.assign()'

    const a = {name: "john"}
    const b = {age: 32}
    result = Object.assign({}, a, b)
    console.log(result);  // Outputs: {name: "john", age: 32}

combine with '...'

    const a = {name: "john"}
    const b = {age: 32}
    result = {...a, ...b}
    console.log(result);  // Outputs: {name: "john", age: 32}

Create a copy of object a

    const copyOfA = {...a}

create copy of array b

    const copyOfB = [...b]

create copy of 'account' object, update the 'balance' to be 3444

    const updatedAccount = { ...account, balance: 3444 }


## ??, ||

- ?? -> undefined or null only
- || -> any 'falsy' value

set 'price' equal to 'salePrice'. if 'salePrice' is 'undefined' set 'price' to 10

    const price = salePrice ?? 10
    // OR
    const price = salePrice || 10

set 'price' equal to 'salePrice'. if 'salePrice' is 'null' set 'price' to 10

    const price = salePrice ?? 10
    // OR
    const price = salePrice || 10

set 'price' equal to 'salePrice'. if 'salePrice' is '0' set 'price' to 10

    const price = salePrice || 10

set 'price' equal to 'salePrice'. if 'salePrice' is 'NaN' set 'price' to 10

    const price = salePrice || 10

set 'price' equal to 'salePrice'. if 'salePrice' is '[]' set 'price' to 10

    const price = salePrice || 10

set 'price' equal to 'salePrice'. if 'salePrice' is 'false' set 'price' to 10

    const price = salePrice || 10

set 'price' equal to 'salePrice'. if 'salePrice' is "" set 'price' to 10

    const price = salePrice || 10

## &&

render react component 'UserCard' only if variable 'user' has been set

    return (
        <div>
            {user && <UserCard user={user} />}
        </div>
    )

render react component 'UserCard' only if 'user' is set, no 'error', and
'isLoggedIn' is true

    return (
        <div>
            {user && error === undefined && isLoggedIn && <UserCard user={user} />}
        </div>
    )

## A ? B : C

write the following on one line using '?:'

    // QUESTION
    if (x === 3) {
        console.log(x - 3)
    } else {
        console.log(x)
    }

    // ANSWER
    x === 3 ? console.log(x-3) : console.log(x)
    // OR
    console.log(x === 3 ? x-3 : x)

render 'UserCard' if 'isLoading' is false, otherwise render 'LoadingIndicator'

    return (
        <div>
            {isLoading ? <LoadingIndicator /> : <UserCard /> }    
        </div>
    )

## formatted string

print "hello USER, it is TIME now", USER and TIME are variables (use '+' only!)

    console.log("hello " + USER + ", it is " + TIME + " now")

print "hello USER, it is TIME now", USER and TIME are variables (formatted string)

    console.log(`hello ${USER}, it is ${TIME} now`)
