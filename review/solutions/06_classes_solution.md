# classes

create a class for a user

    class User {}

notes about classes

- class names should be Caplitalized, camel-case -> 'TimeInterval'
- your editor might give you a warning if you have more than one class per file

export the class so we can use it in other files!

    export class User {}

use this class. create an istance called 'user1'

    const user1 = new User()

## properties

add two properties to the 'User' class: name and age

    // Why does this give an error?
    // we will fix this next session using the 'constructor'
    class User {
        name: string;
        age: number;
    }

set 'age' as optional

    class User {
        name: string;
        age?: number;
    }

set a default value for 'name' to be 'john'

    class User {
        name: string = "john";
        age?: number;
    }

finally, the the errors are gone an we can use the class. create 'user1', an
instance of this class.

- what is the value of 'name' and 'age'.
- change name to '田中'
- change age to 102

like this

    const user1 = new User();
    console.log("name", user1.name)
    console.log("age", user1.age)

    user1.name = "田中"
    user1.age = 102
    console.log("name", user1.name)
    console.log("age", user1.age)

add a 'hidden' property 'id' that defaults to "12345".

- How can we make a hidden attribute in typescript?
- what about javascript?
- what is the different between typescript and javascripts hidden property?
- what is the "'id' is declared but its value is never read" error?
- try and access the 'id'
- try and set the 'id'

like this

    // typescript
    private id: string = "12345"

    // javascript
    #id: string = "12345"

    // difference
    typescripts 'private' is fake. It works while we are developing the code,
    but when you compile to javascript, the property is no longer private! it
    can be accessed at runtime

    javascript '#' is actually private! even at runtime.

    // either way, when you try to access 'id' you get an error

## constructor

add a constructor to the User class (no arguments)

    class User {
      name: string = "john";
      age?: number;
      #id: string = "12345"
      constructor() {}
    }

the constructor is just a function. refer to 05_functions.

add a 'name' argument to the constructor

    constructor(name: string) {}

add an optional 'age' argument

    constructor(name: string, age?: number) {}

the 'id' is still giving a warning because it is never used! log it's value to
the console in the constructor to get rid of the warning.

    constructor(name: string, age?: number) {
        console.log(this.#id)
    }

remove the 'id' default value. instead, set it to a 10 char string generated
from a random number! (you have to do this in the constructor...)

    function generateID(): string {
      // 10 digit number
      const num = Math.random() * 10000000000
      // to string (with no decimal)
      const str = num.toFixed()
      // padded with zeros to 10 characters
      return str.padStart(10, '0')
    }

    class User {
      name: string = "john";
      age?: number;
      #id: string;
      constructor() {
        this.#id = generateID();
        console.log(this.#id);
      }
    }

set the 'name' and 'age' class properties to the values passed into the
constructor

    constructor(name: string, age?: number) {
        this.name = name;
        this.age = age;
        //..
    }

set the class property 'age' to 12 if no value is passed into the constructor.

    constructor(name: string, age?: number) {
        this.name = name;
        this.age = age ?? 12;
        //..
    }

change the constructor to use 'deconstructed object'

    constructor({
        name,
        age,
    }: {
        name: string;
        age?: number;
    }) {
        // ..
    }

## methods

methods are (basically) just functions attached to a class.

add a 'printId' method to User

    class User {
        // ..
        printId() {
            console.log(this.#id)
        }
    }

call this method on your 'user1' object

    user1.printId();

add a 'incrementAgeBy' method. takes 1 number argument. the 'age' property
should be increased by that number amount

    class User {
        // ..
        incrementAgeBy(count: number) {
            this.age += count;
        }
    }

call this method on your 'user1' object

    user1.incrementAgeBy(3);

add a 'getId' method.

    class User {
        // ..
        getId(): string {
            return this.#id;
        }
    }

call this method on your 'user1' object

    const id = user1.getId();

add a private method with javascript. how about typescript.

    class User {
        // ..
        #privateJavascript() {}
        private privateTypescript() {}
    }

## this

'this' is confusing. it can cause a lot of problems. type the following code
into your editor and run it...

    class A {
      name = "class A"
      printName() {
        console.log('this.name ->', this.name);
      }
    }

    class B {
      callback: () => void;
      constructor(callback: () => void) {
        this.callback = callback;
      }
      go() {
        this.callback()
      }
    }

    const a = new A()
    const b = new B(a.printName)

    b.go();

what happens? why!?

    this.name -> undefined

    "this" is the object that calls the function. In this case, the function
    'printName' was renamed as 'callback' and attached to the 'B' class. So,
    the 'this' inside that function refers to 'B' and not 'A' anymore!

fix with 'bind'

    const b = new B(a.printName.bind(a))

    the 'bind' sets the values as the 'this' in the function. Acutally it is
    much more complicated... but I don't know the details...

fix with arrow function

    class A {
      name = "class A"
      printName = () => {
        console.log('this.name ->', this.name);
      }
    }

    Arrow function are 'closures' that capture the variables that you use. in
    this case, it sees that you used 'this' and it saves a reference. Arrow
    functions don't have the 'this' problem.

## inheritance

Just don't do it... (there may be a couple good use-cases... but I can't think
of any... 'events' or 'errors' maybe...)
