# async

## callbacks

"Event" pattern. the 'old' way of async code. When are why is it still useful?

    - Still useful when passing 'handlers' around. Also, handling user actions.
    - code that takes time (API, nework, db, filesystem, etc) -> async/await is
      probably better. (easier to understand)
    - many 'older' api's use this pattern (XMLHttpRequest, IndexedDB, others?)
    - if you see 'onError', 'onLoad', 'on...' -> probably this callback pattern

create a fake 'Button' class. expose a 'onClick' callback method. Use the class.

    type Callback = () => void;
    class Button {
      callback: Callback | undefined;
      async triggerEvent() {
        await new Promise<void>((res) => setTimeout(res, Math.random() * 3000));
        if (this.callback) {
          this.callback();
        }
      }
    }

Use the 'Button' class.

    const start = performance.now();
    const button = new Button();
    button.callback = () => {
      const elapsedSeconds = (performance.now() - start) / 1000;
      console.log("elapse time:", elapsedSeconds, "seconds");
    };
    button.triggerEvent();

create a fake 'accessAPI' function. It takes an 'onFinished' callback.

    async function accessAPI(callback: () => void) {
      // performing something...
      await new Promise<void>((res) => setTimeout(res, Math.random * 3000));
      callback();
    }

Use the 'accessAPI' method

    const start = performance.now();
    accessAPI(() => {
      const elapsed = (performance.now() - start) / 1000;
      console.log("elasped:", elapsed, "seconds");
    });

## promises

'Newer' style of async code.

a 'async' function returns imediately with a 'Promise' object. this object is
initially 'empty', then when the operation is complete, the object value can be
read.

Create a promise that:

- takes 3 seconds to finish.
- creates a random number from 1~10
- returns the number if greater than 5
- otherwise throws an error that says "too small"

like this

    const p = new Promise<number>((resolve, reject) => {
      setTimeout(() => {
        const num = Math.random() * 10;
        if (num > 5) {
          resolve(num);
        } else {
          reject("too small");
        }
      }, 3000);
    });

### .then and .catch

use '.then' and '.catch' to handle the promise

    p.then((value) => {
      console.log("successful number:", value);
    }).catch((msg) => {
      console.error(msg);
    });

### async / await

create an 'async' function that handles the promise using 'await'. how do you
handle the error?

    const handle = async () => {
      try {
        const result = await p;
        console.log("successful results", result);
      } catch (e) {
        console.error("error:", e);
      }
    };
    handle()

### convert Callbacks -> Promises (so we can use async / await) (challenge)

Convert XMLHttpRequest get call to a promise so we can use async / await

using the callback api

    // (old! use 'fetch')
    const url = "http://www.google.com";
    const r = new XMLHttpRequest();
    r.onreadystatechange = () => {
      if (r.readyState !== XMLHttpRequest.DONE) {
        return;
      }
      if (r.status === 200) {
        console.log("finished loading", r.responseText);
        return;
      }
      console.log("finished loading error", r.status);
    };
    r.onerror = (ev: ProgressEvent<EventTarget>) => {
      console.error("loading error", ev);
    };
    r.open("GET", url, true);
    r.send()

convert to promise

    const send = async (url: string): PLromise<string> => {
      return new Promise<string>((resolve, reject) => {
        const r = new XMLHttpRequest();
        r.onreadystatechange = () => {
          if (r.readyState !== XMLHttpRequest.DONE) {
            return;
          }
          if (r.status === 200) {
            return resolve(r.responseText);
          }
          return reject(`finished loading error: ${r.status}`);
        };
        r.onerror = (ev: ProgressEvent<EventTarget>) => {
          reject(`loading error: ${ev}`);
        };
        r.open("GET", url, true);
        r.send();
      });
    };

Use the promisw

    const run = async () => {
      try {
        const responseText = await send("https://www.google.com");
        console.log("success", responseText);
      } catch (e) {
        console.error("error", e);
      }
    };
    run();
