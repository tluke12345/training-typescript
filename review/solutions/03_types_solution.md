# Types

What is a type?

| value                          | type     |
| ------------------------------ | -------- |
| 1                              | number   |
| 1.433                          | number   |
| "hello"                        | string   |
| true                           | boolean  |
| [1,2,3]                        | array    |
| { name: "john", age: 21}       | object   |
| (a:number) => {console.log(a)} | function |

## numbers

define a number with value 20. what is type?

    const num = 20  // type: number

define a number with value 22.22. what is type?

    const num = 22.2    // type: number

define a number with value 2.555e10. what is type?

    const num = 2.555e10    // type number

what is maximium and minimum values of a number?

    const maxNumber = Number.MAX_VALUE
    const minNumber = Number.MIN_VALUE

what is maximum and minimum INTEGER values of number?

    const maxInteger = Number.MAX_SAFE_INTEGER
    const minInteger = Number.MIN_SAFE_INTEGER

what is the difference between the max/min value and max/min integer?

    - value is float, integer is integer.
    - value is approximate, integer is exact.

## special types

what is the 'any' type

    - tells the compiler to ignore the type.
    - removes typechecking -> same as using javascript!
    - easy to add bugs!
    - so... AVOID if possible!

what is the 'unknown' type

    - tells compiler that the type is not known
    - does NOT ignore type
    - type-safe -> force you to check the type before using it!

when do you use 'any'

    - never (guideline not rule)

when do you use 'unknown'

    - anytime you don't know the type

what is undefined? what is null? when do you use them?

    - undefined -> a 'not initialized' or 'not set' value
    - null -> specifically a 'non-number'

    WHEN
    - use undefined...

## enum

create a _numeric_ enum 'Direction' with 4 cardinal directions

    enum Direction {
        North,
        East,
        South,
        West
    }

    console.log(Direction.North); // Outputs 0
    console.log(Direction.East);  // Outputs 1

create a _numberic_ enum 'StatusCode' with some http status codes

    enum StatusCode {
        NotFound = 404,
        Success = 200,
        BadRequest = 400,
        Unauthorized = 401
    }

    console.log(StatusCode.NotFound); // Outputs 404

create a _string_ enum 'Color' with red, green, and blue

    enum Color {
        Red = "RED",
        Green = "GREEN",
        Blue = "BLUE"
    }

    console.log(Color.Red); // Outputs "RED"

what is the difference between a _numberic_ and _string_ enum

    - either is fine!
    - numberic is default, starting at 0. no need to set each value
    - numberic can be easier to use in code ('+', '-', '>', '<', etc.)
    - string can be easier to read and debug
    - string can be easier to convert into JSON

## functions

type of this function: function() {console.log("hello")}

    () => void

type of this function: function(x: string) { return parseInt(x) }

    (x: string) => number
    // NOTE: the type provids NO indication of the possible errors!

type of this function: function(s: string, n: number, b: boolean) { const a = n }

    (s: string, n: number, b: boolean) => void

type of this function: function() { throw new Error("problem") }

    () => never

type of this function: function() { while(true) {console.log("infinite") } }

    () => never

type of this function: () => {console.log("hello") }

    () => void

## checking types

check if variable 'x' is a string

    if (typeof x === "string") { console.log("yes string") }

check if variable 'x' is an integer

    if (Number.isInteger(x)) { console.log("yes integer") }

check if 'error' is of type 'DTPError'

    if (error instanceof DTPError) { console.log("dtp error") }

### typeof vs instanceof

|             | typeof                 | instanceof            |
| ----------- | ---------------------- | --------------------- |
| when to use | primative values       | classes               |
| output      | type as a string       | boolean               |
| usage       | check simple primative | check complex objects |

## coercing types

make typescript think 'x' is a number

    x as number

make typescript think 'y' is object with 'name' property

    type NamedObject = {
        name: string;
    }
    y as NamedObject

use <> notation to make typescript thing 'z' is either a boolean or undefined

    <boolean | undefined> z

## type alias

create a type 'StringOrNumber'

    type StringOrNumber = string | number

create a type 'Employee' with 'name' and 'department'

    type Employee = {
        name: string;
        department: string;
    }

create a type 'Manger' with 'name' and 'teamSize'

    type Manager = {
        name: string;
        teamSize: number;
    }

create a type DeparmentManager that has all the attributes of both 'Employee'
and 'Manager'

    // this is the answer!
    type DepartmentManager = Employee & Manager

    // same as:
    type DepartementManager = {
        name: string;
        department: string;
        teamSize: number;
    }

create a type for 'Account' that has required 'id' and optional 'balance'

    type Account = {
        id: string;
        balance?: number;
    }

    // OR
    type Account = {
        id: string;
        balance: number | undefined;
    }

create a type for 'User' with 'name', 'password', and 'email'. 'email' can be
undefined!

    type User = {
        name: string;
        password: string;
        email: string | undefined;
    }

    // OR
    type User = {
        name: string;
        password: string;
        email?: string;
    }

create a type for an array that has only numbers

    type NumberArray = number[]

create at type for an array that has numbers OR strings

    type NumberOrStringArray = (number | string)[]

create a type for an array that holds any type

    type AnyArray= any[]

## interface

create interface for a 'Vehicle' that can 'accelerate' and 'applyBrakes'

    interface Vehicle {
        accelerate(amount: number): void;
        applyBrakes(): void;
    }

create interface for 'Clock' with 'currentTime' and optional 'changeBatteries'
methods

    interface Clock {
        currentTime(): string;
        changeBatteries?(): void;
    }

apply 'Vehicle' interface to 'Car' class

    class Car implements Vehicle {

        currentSpeed: number = 0

        accelerate(amount:number) {
            currentSpeed += amount
        }

        applyBrakes() {
            currentSpeed = 0
        }
    }

apply 'Clock' interface to 'DigitalClock'

    class DigitalClock implements Clock {

        batteryLevel = 100

        currentTime() {
            batteryLevel -= 5
            const now = new Date()
            return `${now.getHours()}:${now.getMinutes()}`
        }

        changeBatteries() {
            batteryLevel = 100
        }
    }

apply 'Clock' interface to 'AnalogClock' (this clock doesn't have batteries)

    class DigitalClock implements Clock {

        currentTime() {
            const now = new Date()
            return `${now.getHours()}:${now.getMinutes()}`
        }
    }

### interface vs type alias

an interface and type alias are almost the same thing. They can usually be used
interchangeably. They can both specify properties and methods of an object.

My recommendation (guideline not a rule!):

- type alias: properties only -> describe the 'shape' of the data
- interface: methods only -> describe the 'abilities' of object
