# functions

for the rest of the file:

    type User = {
        name: string;
        email: string;
    }

## define

create a function named 'printTime'. print the current time to console

    function printTime() {
        console.log(new Date())
    }

call the function

    printTime()

what is the return type? explicitly add it

    fuction printTime(): void {
        console.log(new Date())
    }

## return value type

create a function named 'getCurrentHour'. it should return the current hour as
a  number

    function getCurrentHour() {
        const now = new Date()
        return now.getHours()
    }

How do we know what the return type is? how can we use VSCode to learn the
return type?

    hover over the function name

explicitly define the return type

    function getCurrentHour(): number {
        const now = new Date()
        return now.getHours()
    }

create a type 'User' and return a user or undefined from the 'getUser' function

    type User = {
        name: string;
        email: string;
    }

    function getUser(): User | undefined {
        //..
    }

create a function that returns a number or a string or an array of strings or
nothing (undefined). why is this helpfu? why is it harmful?

    function soManyReturnTypes(): number | string | stdring[] | undefined {
        // ...
    }

    helpful -> flexibility (we can do what we want!)
    harmful -> flexibility (hard to read, easily becomes complicated)

what error will this give us?: `async function a(): number { //.. }`

    async functions always return promises!
    
    async function a(): Promise<number> { //.. }

consider the following

- explicit: `function a(): number { return 1 }`
- implicit: `function a() { return 1 }`

when and why would we explicitly specify the return type?

    WHY
    - if we don't specify the return type, it is implicitly determined by 
      typescript.
    - if we explicitly define it, we get an error if we return incorrect value
    - if we explicitly define it, other people reading the code (or us in the
      future), have an easier time understanding the code
    - if we explicitly define it, copilot gives better suggestions.
    
    WHEN
    - I think it is always good, but... it is largely a matter of preference...
      so... do whatever you want!

## args

write function 'a' that takes zero arguments

    function a() {}
    a() // call function

write function 'b' that takes one string argument

    function b(str: string) {}
    b("hi") // call function

write function 'c' that takes 2 arguments, a string and a number.

    function c(str: string, num: number) {}
    c("hi", 2) // call function

write function 'd' that takes 1 argument which is a string or number.

    function d(value: string | number) {}

write a function 'setUser' that takes a 'User' argument

- using the 'User' type
- without using the 'User' type (define user inline)

like this

    function setUser(user: User) { // .. }
    function setUser(user: {name: string; email: string;}) { // .. }

## default value

create function 'b' with one string argument, default value is "foo"

    function b(str: string = "foo") {}
    b() // call function with default ("foo")
    b("hi") // call function with "hi"

create function 'c' with 2 arguments, string (default "foo"), number (default 1)

    function c(str: string = foo, num: number = 1) {}
    c("hi", 2) // call function
    c() // string is default "foo", number is default 1
    c("hi") // string is "hi", number is default 1
    c(1) // ERROR -> we have to set string if we want to set number!

try writing this: `function d(str: string = "foo", num: number) {}`. does it
work? try calling it with only a number (and using the default str value)

    we can create this but... we cannot do this:
    d(1)
    
    we have to do this to use the default value!
    d(undefined, 1)

create a function that takes a 'User' object. set the default user to {}

    function setUser(user: User  = {}) { //.. }
    setUser()  // sets user to {}
    setUser({}) // sets user to {}
    setUser({name: "n", email: "e"})

## optional value

write a function with an optional string argument

    function a(value?: string) { }
    // OR
    function a(value: string | undefined)

    a() // OK
    a("s")  // OK

so, the '?' is the same as TYPE | undefined

does this function work: `function b(val1?: string, val2: string) {}`

    No, optional values must be last!

does this function work: `function c(val1: string | undefined, val2: string) {}`

    Yes... I guess '?' and TYPE | undefined are ALMOST the same! 

## destructured object (!important)

create function 'a' that has 1 argument, a 'User' object

    function a(user: User) {}

use this function

    const user = {name: "travis", email: "travis@gmail.com"}
    a(user)
    // OR
    a({name: "travis", email: "travis@gmail.com"})

same as above, but don't use 'User', define inline

    function a(user: { name: string; email: string; }) {}

now 'destructure' the user so we can use 'name' and 'emails' directly!

    function a({name, email}: User) {}

    // OR
    function a({name, email}: {name: string; email: string; }) {}

    // OR
    function a({
        name,
        email,
    }: {
        name: string;
        email: string;
    }) {}

set 'name' with DEFAULT value of "name"

    function a({name = "abc", email}: User) {}

what happens when we try to use the default name `a({email: "travis@gmail.com"})`

    ERROR -> name has a default but it is NOT optional -> it is required when
    we use the function!

set 'name' with default, make 'name' as optional

    function a({
        name = "abc",
        email
    }: {
        name?: string;
        email: string; 
    }) {}

    note: this does NOT work:
    function a({
        name = "abc",
        email
    }: {
        name: string | undefined;
        email: string;
    }) {}

## '...rest'

this is sometimes used in react so lets review.

what is this: `[...a, ...b]`

    combine the values of array 'a' and array 'b' into a single combined array

what is this: `const {a, b, ...z} = {a:1, b:2, c:3, d:4, e:5}`

    object deconstruction:
    - a -> value of 'a'
    - b -> value of 'b'
    - z -> REMAINING VALUES (c, d, e)

these both use the '...' but, this is actually two different operators!

sometimes you will see this in react:

`<BankAccount key={index} {...account} \>`

what is happening the the 'account' variable

    each value in 'account' is set as a JSX property of the same name on the 
    "BankAccount" component

## arrow functions

convert to arrow function `function a() { //.. }`

    const a = () => { //.. }

convert to arrow function `function b(arg1: string) { //.. }`

    const b = (arg1: string) => { //.. }

convert to arrow function `function c(arg1: string): number { //.. }`

    const c = (arg1: string):number => { //.. }

convert to arrow function `async function d(arg1: number): Promise<void> { //.. }`

    const d = async (arg1: number): Promise<void> { //.. }
